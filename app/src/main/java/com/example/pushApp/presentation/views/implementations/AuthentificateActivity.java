package com.example.pushApp.presentation.views.implementations;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;

import com.arellomobile.mvp.MvpAppCompatActivity;
import com.arellomobile.mvp.presenter.InjectPresenter;
import com.example.pushApp.PushAppApplication;
import com.example.pushApp.R;
import com.example.pushApp.managers.interfaces.IUserDataManager;
import com.example.pushApp.presentation.presenters.AuthentificatePresenter;
import com.example.pushApp.presentation.views.interfaces.IAuthentificateView;
import com.firebase.ui.auth.AuthUI;
import com.google.android.gms.common.SignInButton;

import java.util.Arrays;

import javax.inject.Inject;

/**
 * {@link android.app.Activity} аутентификации пользователя.
 * На текущий момент поддерживает аутентификацию через Google аккаунт
 * Эта {@link android.app.Activity} писалась для авторизации в Firebase
 *
 * @author Trikhin P O
 * @since 08.03.2018
 */
public class AuthentificateActivity extends MvpAppCompatActivity implements IAuthentificateView {

    public static final int REQUEST_AUTHORIZATION_CODE = 1;

    @InjectPresenter
    AuthentificatePresenter mPresenter;

    @Inject
    IUserDataManager mUserDataManager;

    private ViewGroup mContentLayout;
    private ProgressBar mProgressBar;

    public static Intent newIntent(Context context) {
        return new Intent(context, AuthentificateActivity.class);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        ((PushAppApplication) getApplication()).getPushAppComponent().inject(this);
        ((PushAppApplication) getApplication()).getPushAppComponent().inject(mPresenter);

        if (mUserDataManager.getFirebaseUID() != null) {
            navigateToMainActivity();
        }

        setContentView(R.layout.activity_authentificate);

        setupGoogleSignInButton();
        findViews();

        setTitle(R.string.authorization_title);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == REQUEST_AUTHORIZATION_CODE) {
            if (resultCode == RESULT_OK) {
                mPresenter.onAuthSuccess();
            } else {
                mPresenter.onAuthError();
            }
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void showGoogleSignInDialog() {
        startActivityForResult(
                AuthUI.getInstance()
                        .createSignInIntentBuilder()
                        .setAvailableProviders(Arrays.asList(
                                new AuthUI.IdpConfig.GoogleBuilder().build()))
                        .setIsSmartLockEnabled(false)
                        .build(),
                REQUEST_AUTHORIZATION_CODE);

    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void navigateToMainActivity() {
        Intent intent = MainActivity.newIntent(this);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NO_ANIMATION);
        startActivity(intent);
        overridePendingTransition(0, 0);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void showErrorDialog() {
        AlertDialog alertDialog = new AlertDialog.Builder(this).create();
        alertDialog.setTitle(getString(R.string.auth_error_dialog_title));
        alertDialog.setMessage(getString(R.string.auth_error_dialog_body));
        alertDialog.show();
    }

    @Override
    public void showLoadingProgressBar(boolean showProgressBar) {
        if (showProgressBar) {
            mProgressBar.setVisibility(View.VISIBLE);
            mContentLayout.setVisibility(View.GONE);
        } else {
            mProgressBar.setVisibility(View.GONE);
            mContentLayout.setVisibility(View.VISIBLE);
        }
    }

    private void setupGoogleSignInButton() {
        SignInButton signInButton = (SignInButton)  findViewById(R.id.google_sign_in_button);
        signInButton.setSize(SignInButton.SIZE_STANDARD);
        signInButton.setOnClickListener(v -> mPresenter.onGoogleSignInCicked());
    }

    private void findViews(){
        mProgressBar = (ProgressBar) findViewById(R.id.progress_bar);
        mContentLayout = (ViewGroup) findViewById(R.id.content_layout);
    }
}
