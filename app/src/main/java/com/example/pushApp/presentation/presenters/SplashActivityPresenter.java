package com.example.pushApp.presentation.presenters;

import com.arellomobile.mvp.InjectViewState;
import com.arellomobile.mvp.MvpPresenter;
import com.example.pushApp.presentation.views.interfaces.ISplashActivityView;

/**
 * @author Trikhin P O
 * @since 14.04.2018
 */

@InjectViewState
public class SplashActivityPresenter extends MvpPresenter<ISplashActivityView> {

    @Override
    protected void onFirstViewAttach() {
        super.onFirstViewAttach();
        getViewState().connectToGoogleFit();
    }

    public void onGoogleFitAccessGranted() {
        getViewState().navigateToMainActivity();
    }
}
