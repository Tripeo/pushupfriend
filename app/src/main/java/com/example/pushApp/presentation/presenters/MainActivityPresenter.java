package com.example.pushApp.presentation.presenters;

import android.util.Log;

import com.arellomobile.mvp.InjectViewState;
import com.arellomobile.mvp.MvpPresenter;
import com.example.pushApp.managers.interfaces.IPreferenceManager;
import com.example.pushApp.presentation.views.interfaces.IMainActivityView;

/**
 * @author Trikhin P O
 */

@InjectViewState
public class MainActivityPresenter extends MvpPresenter<IMainActivityView> {

    private static final String TAG = "MainActivityPresenter";

    private IPreferenceManager mPreferenceManager;

    public MainActivityPresenter(IPreferenceManager preferenceManager) {
        mPreferenceManager = preferenceManager;
        Log.d(TAG,"for debug");
    }

    public void onViewInitialized() {
        Log.d(TAG, "onViewInitialized");
    }
}
