package com.example.pushApp.presentation.views.interfaces;

import com.arellomobile.mvp.GenerateViewState;
import com.arellomobile.mvp.MvpView;

/**
 * MVP View соответсвующий экрану аутентификации
 * Соответствующий MVP Presenter: {@link com.example.pushApp.presentation.presenters.AuthentificatePresenter}
 *
 * @author Trikhin P O
 * @since 08.03.2018
 */

public interface IAuthentificateView extends MvpView {

    /**
     * Показать диалог с аутентификацией через Google аккаунт
     */
    public void showGoogleSignInDialog();

    /**
     * Перейти на главный экран приложения
     */
    public void navigateToMainActivity();

    /**
     * Показывает диалог с ошибкой аутентификации
     */
    public void showErrorDialog();

    /**
     * Показать либо скрыть ProgressBar
     */
    public void showLoadingProgressBar(boolean showProgressBar);

}
