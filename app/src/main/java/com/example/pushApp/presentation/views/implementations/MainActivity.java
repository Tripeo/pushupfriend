package com.example.pushApp.presentation.views.implementations;

import android.Manifest;
import android.accounts.Account;
import android.accounts.AccountManager;
import android.app.Activity;
import android.content.ContentResolver;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.hardware.Sensor;
import android.hardware.SensorManager;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v4.view.ViewPager;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.arellomobile.mvp.MvpAppCompatActivity;
import com.arellomobile.mvp.presenter.InjectPresenter;
import com.arellomobile.mvp.presenter.ProvidePresenter;
import com.example.pushApp.BuildConfig;
import com.example.pushApp.PushAppApplication;
import com.example.pushApp.R;
import com.example.pushApp.SettingsActivity;
import com.example.pushApp.StatisticsActivity;
import com.example.pushApp.TopResultFragment;
import com.example.pushApp.TotalResultFragment;
import com.example.pushApp.TrainingActivity;
import com.example.pushApp.customviews.adapters.MainPagerAdapter;
import com.example.pushApp.dialogs.ChooseExersizeTypeDialog;
import com.example.pushApp.entities.ExersizeType;
import com.example.pushApp.managers.interfaces.IFitnessManager;
import com.example.pushApp.managers.interfaces.IPreferenceManager;
import com.example.pushApp.managers.interfaces.IExersizeSetManager;
import com.example.pushApp.managers.interfaces.IUserDataManager;
import com.example.pushApp.presentation.presenters.MainActivityPresenter;
import com.example.pushApp.presentation.views.interfaces.IMainActivityView;
import com.firebase.ui.auth.AuthUI;
import com.google.android.gms.auth.api.signin.GoogleSignIn;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.fitness.FitnessOptions;
import com.google.android.gms.fitness.data.DataType;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;

import java.util.concurrent.TimeUnit;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class MainActivity extends MvpAppCompatActivity implements View.OnClickListener, IMainActivityView {

    public static int GOOGLE_FIT_PERMISSIONS_REQUEST_CODE = 1;

    private static String TAG = "MainActivity";
    public static final String AUTHORITY = "com.example.android.datasync.provider";
    public static final String ACCOUNT_TYPE = "com.example.android.datasync";
    public static final String ACCOUNT = "default_account";
    private Account mAccount;

    private ViewPager mViewPager;
    @BindView(R.id.exersize_type_text_view)
    TextView mExersizeTypeTextView;
    @BindView(R.id.main_image_view)
    ImageView mMainImageView;
    private MainPagerAdapter mMainPagerAdapter;

    private SensorManager mSensorManager;

    private Sensor mSensor;

    private GoogleApiClient mClient = null;

    @Inject
    IPreferenceManager mPreferenceManager;
    @Inject
    IExersizeSetManager mExersizeSetManager;
    @Inject
    IFitnessManager mFitnessManager;
    @InjectPresenter
    MainActivityPresenter mMainActivityPresenter;
    @Inject
    IUserDataManager mUserDataManager;

    public static Intent newIntent(Context context) {
        return new Intent(context, MainActivity.class);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        ((PushAppApplication)getApplication()).getPushAppComponent().inject(this);
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_main);

        mViewPager = (ViewPager) findViewById(R.id.view_pager);
        ButterKnife.bind(this);

        initViewPager();

        mExersizeTypeTextView.setText(mPreferenceManager.readExersizeType().getName(this));
        switch (mPreferenceManager.readExersizeType()){
            case PUSH_UP:
                mMainImageView.setImageResource(R.drawable.main_img);
                break;
            case CURL:
                mMainImageView.setImageResource(R.drawable.curl);
                break;
        }

        mAccount = CreateSyncAccount(this);

        initSensor();
        initializeGoogleFit();

        mMainActivityPresenter.onViewInitialized();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        if (BuildConfig.DEBUG) {
            getMenuInflater().inflate(R.menu.main_activity_menu, menu);
        }
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onPrepareOptionsMenu(Menu menu) {
        if (BuildConfig.DEBUG) {
            if (ContextCompat.checkSelfPermission(this, Manifest.permission.READ_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED ||
                    ContextCompat.checkSelfPermission(this, Manifest.permission.WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
                menu.findItem(R.id.check_permissions).setEnabled(true);
            } else {
                menu.findItem(R.id.check_permissions).setEnabled(false);
            }
        }
        return super.onPrepareOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.log_item:
                Log.d("delme", mExersizeSetManager.prettyPrintExercizeSets(mPreferenceManager.readExersizeType()));
               // Log.d("delme", mExersizeSetManager.getExersizeSetsOrderedByTime(mPreferenceManager.readExersizeType(),1,1));
                break;
            case R.id.drop_table:
                mExersizeSetManager.dropExersizeTypeTable(mPreferenceManager.readExersizeType());
                break;
            case R.id.check_permissions:
                if (ContextCompat.checkSelfPermission(this, Manifest.permission.READ_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED ||
                        ContextCompat.checkSelfPermission(this, Manifest.permission.WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
                    ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.READ_EXTERNAL_STORAGE, Manifest.permission.WRITE_EXTERNAL_STORAGE}, 1);
                }
                break;
            case R.id.start_biceps_activity:
                Intent intent = new Intent(this, TrainingActivity.class);
                intent.putExtra(TrainingActivity.EXERSIZE_TYPE_KEY, ExersizeType.CURL);
                startActivity(intent);
                break;
            case R.id.init_fitness:
                mFitnessManager.buildFitnessClient(this);
                break;
            case R.id.log_active_subscriptions:
                mFitnessManager.writeData(this,12,1496689961000L,1496693561000L);
                break;
            case R.id.read_active_subscriptions:
                mFitnessManager.readData(this, System.currentTimeMillis()- TimeUnit.DAYS.toMillis(1), System.currentTimeMillis());
                break;
            case R.id.delete_active_subscriptions:
                mFitnessManager.deleteData(this, System.currentTimeMillis() - TimeUnit.DAYS.toMillis(1), System.currentTimeMillis());
                break;
            case R.id.googleFitSync:
                Bundle settingsBundle = new Bundle();
                settingsBundle.putBoolean(
                        ContentResolver.SYNC_EXTRAS_MANUAL, true);
                settingsBundle.putBoolean(
                        ContentResolver.SYNC_EXTRAS_EXPEDITED, true);
                ContentResolver.requestSync(mAccount, AUTHORITY, settingsBundle);
                break;
            case R.id.remove_auth:
                AuthUI.getInstance()
                        .signOut(this)
                        .addOnCompleteListener(task -> {
                            //TODO: на будущее надо переписать это и как то объединить
                            mUserDataManager.cleanFirebaseUID();
                            Intent intent1 = AuthentificateActivity.newIntent(MainActivity.this);
                            intent1.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                            startActivity(intent1);
                        });
                break;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (resultCode == Activity.RESULT_OK) {
            if (requestCode == GOOGLE_FIT_PERMISSIONS_REQUEST_CODE) {
                accessGoogleFit();
            }
        }
    }

    @OnClick(R.id.begin_training_button)
    void onBeginTrainingClicked() {
        Intent intent = new Intent(this, TrainingActivity.class);
        intent.putExtra(TrainingActivity.EXERSIZE_TYPE_KEY, mPreferenceManager.readExersizeType());
        startActivity(intent);
    }

    @OnClick(R.id.statistics_button)
    void onStatisticsButtonClicked() {
        Intent intent = new Intent(this, StatisticsActivity.class);
        startActivity(intent);
    }

    @OnClick(R.id.preference_button)
    void onPreferenceButtonClicked() {
        Intent intent = new Intent(this, SettingsActivity.class);
        startActivity(intent);
    }

    @OnClick(R.id.exersize_type_text_view)
    void onChangeExersizeTypeClicked(){
        ChooseExersizeTypeDialog dialog = ChooseExersizeTypeDialog.newInstance();
        dialog.show(getFragmentManager(),"exersizeTypeDialog");
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.to_right_button:
                mViewPager.setCurrentItem(1);
                break;
            case R.id.to_left_button:
                mViewPager.setCurrentItem(0);
                break;
        }
    }

    @ProvidePresenter
    public MainActivityPresenter providePresenter() {
        return new MainActivityPresenter(mPreferenceManager);
    }

    public Account CreateSyncAccount(Context context) {
        // Create the account type and default account
        Account newAccount = new Account(
                ACCOUNT, ACCOUNT_TYPE);
        // Get an instance of the Android account manager
        AccountManager accountManager =
                (AccountManager) context.getSystemService(
                        ACCOUNT_SERVICE);
        /*
         * Add the account and account type, no password or user data
         * If successful, return the Account object, otherwise report an error.
         */
        if (accountManager.addAccountExplicitly(newAccount, null, null)) {

        } else {

        }
        return newAccount;
    }

    private void initSensor() {
        mSensorManager = (SensorManager) getSystemService(SENSOR_SERVICE);
        mSensor = mSensorManager.getDefaultSensor(Sensor.TYPE_PROXIMITY);

        mExersizeSetManager.setSensorMaxValue(mSensor.getMaximumRange());
    }

    private void initializeGoogleFit () {
        FitnessOptions fitnessOptions = FitnessOptions.builder()
                .addDataType(DataType.TYPE_WORKOUT_EXERCISE, FitnessOptions.ACCESS_READ)
                .build();

        if (!GoogleSignIn.hasPermissions(GoogleSignIn.getLastSignedInAccount(this), fitnessOptions)) {
            Log.d(TAG, "hasnt permissions");
            GoogleSignIn.requestPermissions(
                    this,
                    GOOGLE_FIT_PERMISSIONS_REQUEST_CODE,
                    GoogleSignIn.getLastSignedInAccount(this),
                    fitnessOptions);
        } else {
            Log.d(TAG, "has permissions");
            accessGoogleFit();
        }

    }

    private void accessGoogleFit() {
        Log.d(TAG, "access google fit ");
    }

    private void initViewPager() {
        mMainPagerAdapter = new MainPagerAdapter(getSupportFragmentManager());

        TotalResultFragment totalResultFragment = new TotalResultFragment();
        totalResultFragment.setOnClickListener(this);
        mMainPagerAdapter.addFragment(totalResultFragment);

        TopResultFragment topResultFragment = new TopResultFragment();
        topResultFragment.setOnClickListener(this);
        mMainPagerAdapter.addFragment(topResultFragment);

        mViewPager.setAdapter(mMainPagerAdapter);
    }
}
