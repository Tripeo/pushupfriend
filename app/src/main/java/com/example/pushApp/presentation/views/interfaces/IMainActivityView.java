package com.example.pushApp.presentation.views.interfaces;

import com.arellomobile.mvp.MvpView;

/**
 * @author Trikhin P O
 */

public interface IMainActivityView extends MvpView {
}
