package com.example.pushApp.presentation.presenters;

import com.arellomobile.mvp.InjectViewState;
import com.arellomobile.mvp.MvpPresenter;
import com.example.pushApp.managers.interfaces.IUserDataManager;
import com.example.pushApp.presentation.views.interfaces.IAuthentificateView;
import com.google.firebase.auth.FirebaseAuth;

import javax.inject.Inject;

/**
 * MVP Presenter соответсвующий экрану авторизации
 *
 * @author Trikhin P O
 * @since 08.03.2018
 */

@InjectViewState
public class AuthentificatePresenter extends MvpPresenter<IAuthentificateView> {

    @Inject
    IUserDataManager mUserDataManager;

    @Override
    protected void onFirstViewAttach() {
        super.onFirstViewAttach();
        if (mUserDataManager.getFirebaseUID() != null) {
            getViewState().navigateToMainActivity();
        }
    }

    /**
     * Аутентификация прошла успешно
     */
    public void onAuthSuccess() {
        if (FirebaseAuth.getInstance().getCurrentUser() != null) {
            mUserDataManager.saveFirebaseUID(FirebaseAuth.getInstance().getCurrentUser().getUid());
        }
        getViewState().navigateToMainActivity();
    }

    /**
     * Аутентификация завершилась с ошибкой
     */
    public void onAuthError() {
        getViewState().showLoadingProgressBar(false);
        getViewState().showErrorDialog();
    }

    public void onGoogleSignInCicked() {
        getViewState().showLoadingProgressBar(true);
        getViewState().showGoogleSignInDialog();
    }
}
