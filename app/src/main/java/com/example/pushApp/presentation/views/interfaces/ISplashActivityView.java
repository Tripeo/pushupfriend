package com.example.pushApp.presentation.views.interfaces;

import com.arellomobile.mvp.MvpView;

/**
 * @author Trikhin P O
 * @since 14.04.2018
 */

public interface ISplashActivityView extends MvpView {

    /**
     * Выполняет подключение к сервисам GoogleFit
     */
    public void connectToGoogleFit();

    public void navigateToMainActivity();
}
