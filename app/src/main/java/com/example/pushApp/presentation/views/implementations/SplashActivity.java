package com.example.pushApp.presentation.views.implementations;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;

import com.arellomobile.mvp.MvpAppCompatActivity;
import com.arellomobile.mvp.presenter.InjectPresenter;
import com.example.pushApp.R;
import com.example.pushApp.presentation.presenters.SplashActivityPresenter;
import com.example.pushApp.presentation.views.interfaces.ISplashActivityView;
import com.google.android.gms.auth.api.signin.GoogleSignIn;
import com.google.android.gms.fitness.FitnessOptions;
import com.google.android.gms.fitness.data.DataType;

/**
 * {@link android.app.Activity} приветствия пользователя
 * На ней будет происходить попытка подключения к GoogleFit и последующий переход дальше в приложение.
 */
public class SplashActivity extends MvpAppCompatActivity implements ISplashActivityView {

    private static final int GOOGLE_FIT_PERMISSIONS_REQUEST_CODE = 2;

    @InjectPresenter
    SplashActivityPresenter mPresenter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void connectToGoogleFit() {
        FitnessOptions fitnessOptions = FitnessOptions.builder()
                .addDataType(DataType.TYPE_WORKOUT_EXERCISE, FitnessOptions.ACCESS_READ)
                .addDataType(DataType.TYPE_WORKOUT_EXERCISE, FitnessOptions.ACCESS_WRITE)
                .build();

        if (!GoogleSignIn.hasPermissions(GoogleSignIn.getLastSignedInAccount(this), fitnessOptions)) {
            GoogleSignIn.requestPermissions(
                    this,
                    GOOGLE_FIT_PERMISSIONS_REQUEST_CODE,
                    GoogleSignIn.getLastSignedInAccount(this),
                    fitnessOptions);
        } else {
            mPresenter.onGoogleFitAccessGranted();
        }
    }

    @Override
    public void navigateToMainActivity() {
        startActivity(MainActivity.newIntent(this));
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (resultCode == Activity.RESULT_OK) {
            if (requestCode == GOOGLE_FIT_PERMISSIONS_REQUEST_CODE) {
                mPresenter.onGoogleFitAccessGranted();
            }
        }
    }
}
