package com.example.pushApp.utils;

import android.content.res.Resources;

/**
 * Created by Петр on 27.11.2016.
 */

public class ViewUtils {

    public static float DP_COEFFICIENT = dpToPx(1);

    public static float dpToPx(float dp){
        return (int) (dp * Resources.getSystem().getDisplayMetrics().density);
    }
}
