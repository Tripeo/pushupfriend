package com.example.pushApp.utils;

import java.util.Calendar;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by Петр on 09.01.2017.
 */

public enum Months {

    JANUARY("Январь", Calendar.JANUARY),
    FEBRUARY("Февраль", Calendar.FEBRUARY),
    MARCH("Март", Calendar.MARCH),
    APRIL("Апрель", Calendar.APRIL),
    MAY("Май",Calendar.MAY),
    JUNE("Июнь",Calendar.JUNE),
    JULY("Июль",Calendar.JULY),
    AUGUST("Август",Calendar.AUGUST),
    SEPTEMBER("Сентябрь",Calendar.SEPTEMBER),
    OCTOBER("Октябрь",Calendar.OCTOBER),
    NOVEMBER("Ноябрь",Calendar.NOVEMBER),
    DECEMBER("Декабрь",Calendar.DECEMBER);

    private String mName;
    private int mPos;
    private static Map<Integer,String> sMap = new HashMap<>();

    static {
        for (int i =0; i<Months.values().length;i++){
            sMap.put(Months.values()[i].mPos,Months.values()[i].mName);
        }
    }

    Months(String name, int pos) {
        mName = name;
        mPos = pos;
    }

    public static String getMonthByCalendarPos (int pos){
        return sMap.get(pos);
    }
}
