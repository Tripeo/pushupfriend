package com.example.pushApp;

import android.content.Intent;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.media.AudioAttributes;
import android.media.AudioManager;
import android.media.SoundPool;
import android.os.Build;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.example.pushApp.customviews.PercentCircleView;
import com.example.pushApp.dialogs.SensorInfoDialog;
import com.example.pushApp.entities.ExersizeSet;
import com.example.pushApp.entities.ExersizeType;
import com.example.pushApp.managers.interfaces.IPreferenceManager;
import com.example.pushApp.managers.interfaces.IExersizeSetManager;
import com.example.pushApp.managers.implementations.PreferenceManager;
import com.example.pushApp.presentation.views.implementations.MainActivity;
import com.example.pushApp.utils.ViewUtils;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class TrainingActivity extends AppCompatActivity implements SensorEventListener {

    public static final String PUSH_UP_COUNT_KEY = "pushUpCount";

    public static String EXERSIZE_TYPE_KEY = "exersizeTypeKey";

    @Inject
    IExersizeSetManager mExersizeSetManager;
    @Inject
    IPreferenceManager mPreferenceManager;

    private SensorManager mSensorManager;
    private Sensor mSensor;

    private SoundPool mSoundPool;
    private final int maxSoundStreams = 4;
    private int soundIdIncrement = -1;

    @BindView(R.id.push_up_count_text_view)
    TextView mPushUpCountTextView;
    @BindView(R.id.percent_circle_view)
    PercentCircleView mPercentCircleView;
    @BindView(R.id.plus_button)
    Button mPlusButton;
    @BindView(R.id.menos_button)
    Button mMenosButton;
    @BindView(R.id.next_button)
    Button mNextButton;
    @BindView(R.id.best_result_text_view)
    TextView mBestResultTextView;

    private boolean isBodyOnFloor = false;
    private boolean isCurlOnBottom = true;

    private int mPushCount = 0;
    private int mPushUpRecordValue = 0;

    private TrainingStage mStage = TrainingStage.TRAINING_IN_PROCESS_STAGE;

    private ExersizeType mExersizeType;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        ((PushAppApplication)getApplication()).getPushAppComponent().inject(this);

        mExersizeType = (ExersizeType) getIntent().getSerializableExtra(EXERSIZE_TYPE_KEY);

        setContentView(R.layout.activity_training);

        setTitle(getString(R.string.fast_training));

        mSensorManager = (SensorManager)  getSystemService(SENSOR_SERVICE);
        switch (mExersizeType){
            case PUSH_UP:
                mSensor = mSensorManager.getDefaultSensor(Sensor.TYPE_PROXIMITY);
                break;
            case CURL:
                mSensor = mSensorManager.getDefaultSensor(Sensor.TYPE_GRAVITY);
                break;
        }

        if(mExersizeSetManager.getAbsolutelyMaximumSet(mPreferenceManager.readExersizeType())!=null){
            mPushUpRecordValue = mExersizeSetManager.getAbsolutelyMaximumSet(mPreferenceManager.readExersizeType()).getCount();
        }
        ButterKnife.bind(this);

        mPercentCircleView.setColor(getResources().getColor(R.color.colorPrimary));
        mPercentCircleView.setStrokeWidth(ViewUtils.dpToPx(2));

        if (mPushUpRecordValue!=0){
            mBestResultTextView.setVisibility(View.VISIBLE);
            mBestResultTextView.setText(getString(R.string.you_best_result) +" " +mPushUpRecordValue);
        } else {
            mBestResultTextView.setVisibility(View.GONE);
        }

        setupSoundPool();

    }

    @Override
    protected void onResume() {
        super.onResume();
        if (mPreferenceManager.readBoolean(PreferenceManager.MANUAL_MUST_SHOWN_KEY,true)){
            SensorInfoDialog sensorInfoDialog = SensorInfoDialog.newInstance(false);
            sensorInfoDialog.show(getFragmentManager(),"sensor_dialog");
        }
    }

    @Override
    protected void onStart() {
        super.onStart();
        mSensorManager.registerListener(this,mSensor,SensorManager.SENSOR_DELAY_FASTEST);
    }

    @Override
    protected void onStop() {
        super.onStop();
        mSensorManager.unregisterListener(this);
    }

    @Override
    protected void onPause() {
        super.onPause();
    }

    @OnClick(R.id.next_button)
    void onNextButtonClicked(){

        switch (mStage){
            case TRAINING_IN_PROCESS_STAGE:
                mPlusButton.setVisibility(View.VISIBLE);
                mMenosButton.setVisibility(View.VISIBLE);
                mStage = TrainingStage.RESULT_STAGE;
                mNextButton.setText(R.string.back_to_main);
                break;
            case RESULT_STAGE:
                if (mPushCount > 0){
                    ExersizeSet set = new ExersizeSet(mPreferenceManager.readExersizeType(), ExersizeSet.DEFAULT_ID, mPushCount,System.currentTimeMillis());
                    mExersizeSetManager.writeExersizeSet(set);
                }
                Intent intent = new Intent(this,MainActivity.class);
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(intent);
                break;
        }
    }

    @OnClick(R.id.plus_button)
    void onPlusButtonClicked(){
        mPushCount++;
        setPushCount(mPushCount);
    }

    @OnClick(R.id.menos_button)
    void onMenosButtonClicked(){
        mPushCount--;
        if (mPushCount <0){
            mPushCount=0;
        }
        setPushCount(mPushCount);
    }

    @OnClick(R.id.about_image_button)
    void onAboutImageButtonClicked(){
        SensorInfoDialog.newInstance(false).show(getFragmentManager(),"about_info");
    }

    @Override
    public void onSensorChanged(SensorEvent sensorEvent) {
        switch (mExersizeType) {
            case PUSH_UP:
                if (isBodyOnFloor){
                    if (sensorEvent.values[0] >= Math.min(mExersizeSetManager.getSensorMaxValue(),mPreferenceManager.readInt(PreferenceManager.TOP_TRESHOLD_PREFERENCE_KEY,PreferenceManager.DEFAULT_TOP_TRESHOLD_VALUE))){
                        isBodyOnFloor = false;
                        mPushCount++;
                        setPushCount(mPushCount);
                        playIncrementSound();
                    }
                } else {
                    if (sensorEvent.values[0] <= Math.max(0,mPreferenceManager.readInt(PreferenceManager.BOTTOM_TRESHOLD_PREFERENCE_KEY,0))){
                        isBodyOnFloor = true;
                    }
                }
                break;
            case CURL:
                if (isCurlOnBottom){
                    if (sensorEvent.values[1] > 2){
                        isCurlOnBottom = false;
                        mPushCount++;
                        setPushCount(mPushCount);
                        playIncrementSound();
                    }
                } else {
                    if (sensorEvent.values[1] < -2){
                        isCurlOnBottom = true;
                    }
                }
                for (int i =0; i< sensorEvent.values.length;i++){
                    Log.d("delme","value " + i + " is " + sensorEvent.values[i] +"\n");
                }
                break;
        }
    }

    @Override
    public void onAccuracyChanged(Sensor sensor, int i) {

    }

    private void playIncrementSound(){
        mSoundPool.play(soundIdIncrement,1,1,0,0,1);
    }

    private void setPushCount(int i){
        mPushUpCountTextView.setText(i+"");
        if (mPushUpRecordValue!=0){
            if (i>mPushUpRecordValue){
                mPercentCircleView.setPercent(100);
            } else {
                mPercentCircleView.setPercent((float)i/mPushUpRecordValue);
            }
        }
    }

    private enum TrainingStage {
        TRAINING_IN_PROCESS_STAGE,
        RESULT_STAGE;
    }

    private void setupSoundPool(){
        if (Build.VERSION.SDK_INT >= 21 ){
            SoundPool.Builder builder = new SoundPool.Builder();
            builder.setMaxStreams(maxSoundStreams);
            AudioAttributes attributes = new AudioAttributes.Builder()
                    .setUsage(AudioAttributes.USAGE_ASSISTANCE_SONIFICATION)
                    .setContentType(AudioAttributes.CONTENT_TYPE_SONIFICATION)
                    .build();
            builder.setAudioAttributes(attributes);
            mSoundPool = builder.build();
        } else {
            mSoundPool = new SoundPool(maxSoundStreams, AudioManager.STREAM_MUSIC,0);
        }

        soundIdIncrement = mSoundPool.load(this,R.raw.increment_sound,1);
    }
}
