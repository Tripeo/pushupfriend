package com.example.pushApp;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.TextView;

import com.example.pushApp.customviews.HistogramView;
import com.example.pushApp.customviews.adapters.DetailExersizeListAdapter;
import com.example.pushApp.customviews.adapters.IOnClickBarListener;
import com.example.pushApp.entities.BarEntity;
import com.example.pushApp.entities.ExercizeBarEntity;
import com.example.pushApp.entities.ExersizeSet;
import com.example.pushApp.managers.interfaces.IPreferenceManager;
import com.example.pushApp.managers.interfaces.IExersizeSetManager;

import java.util.List;

import javax.inject.Inject;

public class StatisticsActivity extends AppCompatActivity implements IOnClickBarListener{

    private HistogramView mHistogramView;

    @Inject
    IExersizeSetManager mExersizeSetManager;
    @Inject
    IPreferenceManager mPreferenceManager;

    private RecyclerView mDetailExersizeListRecyclerView;
    private DetailExersizeListAdapter mAdapter;
    private TextView mChooseDayInfoTextView;

    private BarEntity lastSelectedBarEntity = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        ((PushAppApplication)getApplication()).getPushAppComponent().inject(this);

        setContentView(R.layout.activity_statistics);

        setTitle(getString(R.string.statistics));

        mHistogramView = (HistogramView) findViewById(R.id.histogramm_view);

        List<ExersizeSet> mySets = mExersizeSetManager.getAllExersizeSets(mPreferenceManager.readExersizeType());
        mHistogramView.setData(mySets);

        mHistogramView.setOnClickBarListener(this);

        mDetailExersizeListRecyclerView = (RecyclerView)findViewById(R.id.detail_exercizes_recycler_view);
        mDetailExersizeListRecyclerView.setLayoutManager(new LinearLayoutManager(this));
        mAdapter = new DetailExersizeListAdapter();
        mDetailExersizeListRecyclerView.setAdapter(mAdapter);

        mChooseDayInfoTextView = (TextView) findViewById(R.id.choose_day_info_text_view);
    }

    @Override
    public void onBarClicked(View view) {
        int pos = mHistogramView.getRecyclerView().getChildAdapterPosition(view);
        long time = ((ExercizeBarEntity)mHistogramView.getAdapter().getData().get(pos)).getTimeInMillis();
        // пробрасываем данные на детальный список упражнений
        mAdapter.setExersizeData(mExersizeSetManager.getDayExersizeSets(mPreferenceManager.readExersizeType(), time),time,view.getContext());

        BarEntity barEntity = mHistogramView.getAdapter().getData().get(pos);

        if (lastSelectedBarEntity!=null){
            lastSelectedBarEntity.setSelected(false);
        }
        barEntity.setSelected(true);
        lastSelectedBarEntity = barEntity;

        mHistogramView.getRecyclerView().getAdapter().notifyDataSetChanged();

        mChooseDayInfoTextView.setVisibility(View.GONE);
        mDetailExersizeListRecyclerView.setVisibility(View.VISIBLE);

    }
}
