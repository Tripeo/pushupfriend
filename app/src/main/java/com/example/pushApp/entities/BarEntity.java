package com.example.pushApp.entities;

/**
 * Created by Петр on 20.12.2016.
 */

public class BarEntity {

    private int mValue;
    private int xCord;
    private int bottomInteger;
    private boolean isSelected;

    public BarEntity(int value, int xCord, int bottomInteger) {
        mValue = value;
        this.xCord = xCord;
        this.bottomInteger = bottomInteger;
    }

    public int getValue() {
        return mValue;
    }

    public void setValue(int value) {
        mValue = value;
    }

    public int getxCord() {
        return xCord;
    }

    public void setxCord(int xCord) {
        this.xCord = xCord;
    }

    public int getBottomInteger() {
        return bottomInteger;
    }

    public boolean isSelected() {
        return isSelected;
    }

    public void setSelected(boolean selected) {
        isSelected = selected;
    }

    public void setBottomInteger(int bottomInteger) {
        this.bottomInteger = bottomInteger;
    }
}
