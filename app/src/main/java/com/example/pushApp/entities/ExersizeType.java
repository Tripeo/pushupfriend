package com.example.pushApp.entities;

import android.app.Application;
import android.content.Context;

import com.example.pushApp.R;

import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by Петр on 22.11.2016.
 */

public enum ExersizeType implements Serializable {
    COMMON_TYPE(R.string.common_type),
    PUSH_UP(R.string.push_up_type),
    PULLUP(R.string.pull_up_type),
    CURL(R.string.curl_type);

    private int resourceName;

    ExersizeType(int resname) {
        resourceName = resname;
    }

    private static Map<String, ExersizeType> sMap;

    static {
        sMap = new HashMap<>();
        for (ExersizeType type : ExersizeType.values()){
            sMap.put(type.name(),type);
        }
    }

    public static ExersizeType getTypeByString(String type){
        return sMap.get(type);
    }

    public static ExersizeType getDefaultType(){
        return PUSH_UP;
    }

    public String getName(Context context) {
        return context.getResources().getString(resourceName);
    }
}
