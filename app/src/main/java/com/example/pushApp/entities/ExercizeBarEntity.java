package com.example.pushApp.entities;

/**
 * Created by Петр on 09.01.2017.
 */

public class ExercizeBarEntity extends BarEntity {

    private long mTimeInMillis = 0;

    public ExercizeBarEntity(int value, int xCord, int bottomInteger) {
        super(value, xCord, bottomInteger);
    }

    public long getTimeInMillis() {
        return mTimeInMillis;
    }

    public void setTimeInMillis(long timeInMillis) {
        mTimeInMillis = timeInMillis;
    }
}
