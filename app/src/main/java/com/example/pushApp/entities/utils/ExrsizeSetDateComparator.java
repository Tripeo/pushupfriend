package com.example.pushApp.entities.utils;

import com.example.pushApp.entities.ExersizeSet;

import java.util.Comparator;

/**
 * Created by Петр on 12.12.2016.
 * Сортировка по убыванию даты с самой новой и вниз
 */

public class ExrsizeSetDateComparator implements Comparator<ExersizeSet> {
    @Override
    public int compare(ExersizeSet exersizeSet1, ExersizeSet exersizeSet2) {
        if (exersizeSet1.getEndDate()>exersizeSet2.getEndDate()){
            return 1;
        } else if (exersizeSet1.getEndDate() < exersizeSet2.getEndDate()){
            return -1;
        } else {
            return 0;
        }
    }
}
