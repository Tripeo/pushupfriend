package com.example.pushApp.entities;

import java.util.Comparator;

/**
 * Created by Петр on 22.11.2016.
 */

public class ExersizeSet implements Comparator {

    public static final int DEFAULT_ID = -1;

    private ExersizeType mType;
    private int mCount;
    private long mEndDate;
    private long mStartDate;
    private int id;

    public ExersizeSet(ExersizeType type, int id, int count, long endDate) {
        mType = type;
        mCount = count;
        mEndDate = endDate;
        this.id = id;
    }

    public int getCount() {
        return mCount;
    }

    public void setCount(int count) {
        this.mCount = count;
    }

    public long getStartDate() {
        return mStartDate;
    }

    public void setStartDate(long startDate) {
        mStartDate = startDate;
    }

    public long getEndDate() {
        return mEndDate;
    }

    public void setEndDate(long endDate) {
        this.mEndDate = endDate;
    }

    public ExersizeType getType() {
        return mType;
    }

    public void setType(ExersizeType type) {
        mType = type;
    }

    @Override
    public String toString() {
        return "Exersize: type = " + mType.name() +"; count = " +mCount+"; date = " + mEndDate;
    }

    @Override
    public int compare(Object o, Object t1) {
        return 0;
    }
}
