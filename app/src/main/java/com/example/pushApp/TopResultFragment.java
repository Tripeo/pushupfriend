package com.example.pushApp;


import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.TextView;

import com.example.pushApp.entities.ExersizeSet;
import com.example.pushApp.managers.interfaces.IPreferenceManager;
import com.example.pushApp.managers.interfaces.IExersizeSetManager;
import com.example.pushApp.utils.Months;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.List;

import javax.inject.Inject;


/**
 * A simple {@link Fragment} subclass.
 */
public class TopResultFragment extends Fragment implements View.OnClickListener {

    private static String TOP_RESULT_DATE_FORMAT = "dd MMMM yyyy";
    private static String TOP_MONTH_DATE_FORMAT = "yyyy";
    private static String TOP_DAY_DATE_FORMAT = "dd MMMM yyyy";

    private TextView mBestResultTextView;
    private TextView mBestResultDateTextView;
    private TextView mTopMonthTextView;
    private TextView mTopMonthDateTextView;
    private TextView mTopDayTextView;
    private TextView mTopDayDateTextView;

    private ImageButton mToLeftImageButton;
    private View.OnClickListener mOnClickListener;

    @Inject
    IExersizeSetManager mExersizeSetManager;
    @Inject
    IPreferenceManager mPreferenceManager;

    public TopResultFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        ((PushAppApplication)getActivity().getApplication()).getPushAppComponent().inject(this);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View resultView = inflater.inflate(R.layout.best_results_fragment_layout, container, false);

        mBestResultTextView = (TextView) resultView.findViewById(R.id.best_result_text_view);
        mBestResultDateTextView = (TextView) resultView.findViewById(R.id.best_result_date_text_view);
        mTopMonthTextView = (TextView) resultView.findViewById(R.id.best_month_text_view);
        mTopMonthDateTextView = (TextView) resultView.findViewById(R.id.best_month_date_text_view);
        mTopDayTextView = (TextView) resultView.findViewById(R.id.max_day_text_view);
        mTopDayDateTextView = (TextView) resultView.findViewById(R.id.max_day_date_text_view);

        mToLeftImageButton = (ImageButton) resultView.findViewById(R.id.to_left_button);

        bindViews();
        return resultView;
    }

    @Override
    public void onClick(View v) {
        if (mOnClickListener!=null){
            mOnClickListener.onClick(v);
        }
    }

    public void setOnClickListener(View.OnClickListener onClickListener) {
        mOnClickListener = onClickListener;
    }

    private void bindViews(){

        mToLeftImageButton.setOnClickListener(this);

        SimpleDateFormat simpleDateFormat = new SimpleDateFormat(TOP_RESULT_DATE_FORMAT);

        //Лучший подход
        ExersizeSet exersizeSet = mExersizeSetManager.getAbsolutelyMaximumSet(mPreferenceManager.readExersizeType());
        if (exersizeSet != null) {
            mBestResultTextView.setText(getString(R.string.your_record) + " " + exersizeSet.getCount());
            mBestResultDateTextView.setText(updateWithBracers(simpleDateFormat.format(new Date(exersizeSet.getEndDate()))));
        } else {
            mBestResultTextView.setText(getString(R.string.your_record) + " " + 0);
        }

        //Лучший месяц
        List<ExersizeSet> topMonthSet = mExersizeSetManager.getBestMonthExersizeSet(mPreferenceManager.readExersizeType());
        long topMonthCount = 0;
        for (ExersizeSet set : topMonthSet) {
            topMonthCount = topMonthCount + set.getCount();
        }
        mTopMonthTextView.setText(getString(R.string.best_month) + " " + topMonthCount);
        if (topMonthSet.size()>0){
            Calendar tempCalendar = new GregorianCalendar();
            tempCalendar.setTimeInMillis(topMonthSet.get(0).getEndDate());
            String month = Months.getMonthByCalendarPos(tempCalendar.get(Calendar.MONTH));
            simpleDateFormat = new SimpleDateFormat(TOP_MONTH_DATE_FORMAT);
            mTopMonthDateTextView.setText(updateWithBracers(month.toLowerCase() + " " +simpleDateFormat.format(new Date(topMonthSet.get(0).getEndDate()))));
        }

        //Лучший день
        List<ExersizeSet> topDaySets = mExersizeSetManager.getMaximumDayExersizeSet(mPreferenceManager.readExersizeType());
        long topDayCount = 0;
        for (ExersizeSet set : topDaySets) {
            topDayCount = topDayCount + set.getCount();
        }
        mTopDayTextView.setText(getString(R.string.top_day) + " " + topDayCount);
        if (topDaySets.size()>0){
            simpleDateFormat = new SimpleDateFormat(TOP_DAY_DATE_FORMAT);
            mTopDayDateTextView.setText(updateWithBracers(simpleDateFormat.format(new Date(topDaySets.get(0).getEndDate()))));
        }
    }

    private String updateWithBracers(String incomingString){
        return "("+incomingString+")";
    }


}
