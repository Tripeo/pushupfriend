package com.example.pushApp.asyncAdapter;

import android.app.Service;
import android.content.Intent;
import android.os.IBinder;
import android.support.annotation.Nullable;

/**
 * Сервис необходимы для фреймворка AsyncAdapter
 * Он инициализирует {@link Authentificator}
 * И имеет к нему доступ
 *
 * @author Trikhin P O
 */

public class AuthentificatorService extends Service {

    private Authentificator mAuthentificator;

    @Override
    public void onCreate() {
        super.onCreate();
        mAuthentificator = new Authentificator(this);
    }

    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return mAuthentificator.getIBinder();
    }
}
