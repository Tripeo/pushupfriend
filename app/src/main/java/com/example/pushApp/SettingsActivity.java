package com.example.pushApp;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageButton;
import android.widget.SeekBar;
import android.widget.TextView;

import com.example.pushApp.dialogs.FromAuthorDialog;
import com.example.pushApp.dialogs.SensorInfoDialog;
import com.example.pushApp.managers.interfaces.IPreferenceManager;
import com.example.pushApp.managers.interfaces.IExersizeSetManager;
import com.example.pushApp.managers.implementations.PreferenceManager;

import javax.inject.Inject;

public class SettingsActivity extends AppCompatActivity implements View.OnClickListener {

    private ImageButton mSettingsInfoButton;
    private ImageButton mFromAuthorSettingButton;
    private TextView mTopSeekBarTextView;
    private SeekBar mTopSeekBar;
    private TextView mBottomSeekBarTextView;
    private SeekBar mBottomSeekbar;

    @Inject
    IPreferenceManager mPreferenceManager;
    @Inject
    IExersizeSetManager mExersizeSetManager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        ((PushAppApplication)getApplication()).getPushAppComponent().inject(this);

        setContentView(R.layout.activity_settings);

        mSettingsInfoButton = (ImageButton) findViewById(R.id.sensor_info_button);
        mSettingsInfoButton.setOnClickListener(this);

        mFromAuthorSettingButton = (ImageButton) findViewById(R.id.from_author_info);
        mFromAuthorSettingButton.setOnClickListener(this);

        mTopSeekBarTextView = (TextView) findViewById(R.id.top_seekbar_value_text_view);
        mTopSeekBar = (SeekBar) findViewById(R.id.top_treshold_seekBar);
        mBottomSeekBarTextView = (TextView) findViewById(R.id.bottom_seekbar_value_text_view);
        mBottomSeekbar = (SeekBar) findViewById(R.id.bottom_treshold_seekBar);

        setTitle(getString(R.string.settings));

        bindViews();
    }

    @Override
    protected void onStop() {
        super.onStop();
        mPreferenceManager.writeInt(PreferenceManager.TOP_TRESHOLD_PREFERENCE_KEY,mTopSeekBar.getProgress());
        mPreferenceManager.writeInt(PreferenceManager.BOTTOM_TRESHOLD_PREFERENCE_KEY,mBottomSeekbar.getProgress());
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.sensor_info_button:
                SensorInfoDialog dialog = SensorInfoDialog.newInstance(true);
                dialog.show(getFragmentManager(),"sensor_dialog");
                break;
            case R.id.from_author_info:
                FromAuthorDialog fromAuthorDialog = FromAuthorDialog.newInstance();
                fromAuthorDialog.show(getFragmentManager(),"from_author_dialog");
                break;
        }
    }

    private void bindViews(){
        mTopSeekBar.setMax((int) mExersizeSetManager.getSensorMaxValue());
        mTopSeekBar.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                mTopSeekBarTextView.setText(progress+" "+getString(R.string.centimeters));
                if(progress < mBottomSeekbar.getProgress()){
                    mBottomSeekbar.setProgress(progress);
                }
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {

            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {

            }
        });


        mBottomSeekbar.setMax((int) mExersizeSetManager.getSensorMaxValue());
        mBottomSeekbar.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                mBottomSeekBarTextView.setText(progress+" "+getString(R.string.centimeters));
                if(progress > mTopSeekBar.getProgress()){
                    mTopSeekBar.setProgress(progress);
                }
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {

            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {

            }
        });

        mTopSeekBar.setProgress(mPreferenceManager.readInt(PreferenceManager.TOP_TRESHOLD_PREFERENCE_KEY,PreferenceManager.DEFAULT_TOP_TRESHOLD_VALUE));
        mBottomSeekbar.setProgress(mPreferenceManager.readInt(PreferenceManager.BOTTOM_TRESHOLD_PREFERENCE_KEY,0));

        mTopSeekBarTextView.setText(mTopSeekBar.getProgress()+" "+getString(R.string.centimeters));
        mBottomSeekBarTextView.setText(mBottomSeekbar.getProgress()+" "+getString(R.string.centimeters));
    }
}
