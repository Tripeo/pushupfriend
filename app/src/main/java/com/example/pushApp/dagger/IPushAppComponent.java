package com.example.pushApp.dagger;

import com.example.pushApp.managers.interfaces.IUserDataManager;
import com.example.pushApp.presentation.presenters.AuthentificatePresenter;
import com.example.pushApp.presentation.presenters.SplashActivityPresenter;
import com.example.pushApp.presentation.views.implementations.AuthentificateActivity;
import com.example.pushApp.presentation.views.implementations.MainActivity;
import com.example.pushApp.SettingsActivity;
import com.example.pushApp.StatisticsActivity;
import com.example.pushApp.TopResultFragment;
import com.example.pushApp.TotalResultFragment;
import com.example.pushApp.TrainingActivity;
import com.example.pushApp.TrainingResultActivity;
import com.example.pushApp.dialogs.ChooseExersizeTypeDialog;
import com.example.pushApp.dialogs.SensorInfoDialog;

import javax.inject.Singleton;
import dagger.Component;

/**
 * Created by Петр on 11.05.2017.
 */


@Component (modules = {CoreModule.class})
@Singleton
public interface IPushAppComponent {

    void inject (SettingsActivity settingsActivity);
    void inject (MainActivity mainActivity);
    void inject(ChooseExersizeTypeDialog dialog);
    void inject(SensorInfoDialog sensorInfoDialog);
    void inject (StatisticsActivity statisticsActivity);
    void inject (TopResultFragment topResultFragment);
    void inject (TotalResultFragment totalResultFragment);
    void inject (TrainingActivity trainingActivity);
    void inject (TrainingResultActivity trainingResultActivity);
    void inject (AuthentificateActivity authentificateActivity);

    void inject (AuthentificatePresenter presenter);
    void inject (SplashActivityPresenter presenter);

}
