package com.example.pushApp.dagger;

import android.content.Context;
import android.support.annotation.NonNull;

import com.example.pushApp.databases.PushUpDBOpenHelper;
import com.example.pushApp.managers.implementations.ExersizeSetManager;
import com.example.pushApp.managers.implementations.FitnesManager;
import com.example.pushApp.managers.implementations.RealTImeDatabaseHelper;
import com.example.pushApp.managers.implementations.UserDataManager;
import com.example.pushApp.managers.interfaces.IExersizeSetManager;
import com.example.pushApp.managers.interfaces.IFitnessManager;
import com.example.pushApp.managers.interfaces.IPreferenceManager;
import com.example.pushApp.managers.implementations.PreferenceManager;
import com.example.pushApp.managers.interfaces.IRealTimeDatabaseHelper;
import com.example.pushApp.managers.interfaces.IUserDataManager;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;

/**
 * Created by Петр on 11.05.2017.
 */

@Module
public class CoreModule {

    private Context mContext;

    public CoreModule(Context context) {
        mContext = context;
    }

    @Singleton
    @Provides
    @NonNull
    IPreferenceManager getPreferenseManager() {
        return new PreferenceManager(mContext);
    }

    @Singleton
    @Provides
    @NonNull
    IExersizeSetManager getExersizeSetManager(PushUpDBOpenHelper pushUpDBOpenHelper) {
        return new ExersizeSetManager(pushUpDBOpenHelper);
    }

    @Singleton
    @Provides
    @NonNull
    IFitnessManager getFitnessManager() {
        return new FitnesManager(mContext);
    }

    @Singleton
    @Provides
    @NonNull
    PushUpDBOpenHelper getPushUpDBOpenHelper() {
        return new PushUpDBOpenHelper(mContext);
    }

    @Singleton
    @Provides
    @NonNull
    IUserDataManager getUserDataManager(IPreferenceManager preferenceManager) {
        return new UserDataManager(preferenceManager);
    }

    @Singleton
    @Provides
    @NonNull
    IRealTimeDatabaseHelper getRealTimeDataBaseHelper() {
        return new RealTImeDatabaseHelper();
    }

}
