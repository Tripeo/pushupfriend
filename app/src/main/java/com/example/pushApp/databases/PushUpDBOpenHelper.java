package com.example.pushApp.databases;

import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import com.example.pushApp.entities.ExersizeType;

import java.util.HashMap;
import java.util.Locale;
import java.util.Map;

/**
 * Created by Петр on 07.11.2016.
 */

public class PushUpDBOpenHelper extends SQLiteOpenHelper {

    public static final long TIME_BETWEEN_PUSH_UP = 400;

    public static final String INTEGER_TYPE = "INTEGER";

    private static final int DATABASE_VERISON = 4;
    private static final String DATABASE_NAME = "pushUpDatabase";

    public static final String PUSH_UP_TABLE_NAME = "pushUpTable";
    public static final String CURL_TABLE_NAME = "curlTable";

    public static final String ID_COLUMN = "id";
    public static final String PUSH_UP_COUNT_COLUMN = "pushUpCount";
    public static final String PUSH_UP_STARTDATE_COLUMN = "pushUpStartDate";
    public static final String PUSH_UP_FINISH_DATE_COLUMN = "pushUpDate";
    public static final String GOOGLE_FIT_STATUS_COLLUMN = "googleFitSynched";

    public static final String GOOGLE_FIT_DEFAULT_VALUE = "0";
    public static final String START_DATE_DEFAULT_VALUE = "0";

    private static final String CREATE_TABLE_REQUEST = "create table %s (" +
            ID_COLUMN + " integer primary key autoincrement, " +
            PUSH_UP_COUNT_COLUMN + " integer, " +
            PUSH_UP_FINISH_DATE_COLUMN + " integer " +
            ");";

    public static final String SELECT_ALL_STRINGS = "select * from %s";
    public static final String SELECT_MAX_PUSH_UP_EXERSIZE_SET = "select * from %s order by " + PUSH_UP_COUNT_COLUMN + " desc limit 1";
    public static final String SELECT_SUM_OF_ALL_PUSHES = " select sum(" + PUSH_UP_COUNT_COLUMN + ") from %s";
    public static final String DROP_PUSH_UP_TABLE = "delete from %s";
    public static final String SELECT_MONT_SUM_OF_ALL_PUSHES = " select sum(" + PUSH_UP_COUNT_COLUMN + ") from %s where " + PUSH_UP_FINISH_DATE_COLUMN + " > ?";
    public static final String SELECT_ALL_STRINGS_ORDERED_BY_TIME = "select * from %s order by " + PUSH_UP_FINISH_DATE_COLUMN + " desc";
    public static final String SELECT_STRINGS_ORDERED_BY_TIME = "select * from %s order by " + PUSH_UP_FINISH_DATE_COLUMN + " desc limit %d offset %d where " + GOOGLE_FIT_STATUS_COLLUMN + " = 0";
    public static final String ADD_GOOGLE_FIT_STATUS_COLLUMN = "alter table %s add column %s %s default %s";
    public static final String UPDATE_COLLUMN_WITH_NEW_VALUE = "UPDATE %s SET %s=%s WHERE id=%s ";

    /**
     * Карта с названиями таблиц для кажого типа упражнений
     */
    private static Map<ExersizeType, String> sMap;
    static {
        sMap = new HashMap<>();
        sMap.put(ExersizeType.PUSH_UP, PUSH_UP_TABLE_NAME);
        sMap.put(ExersizeType.CURL, CURL_TABLE_NAME);
    }

    public PushUpDBOpenHelper(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERISON);
    }

    @Override
    public void onCreate(SQLiteDatabase sqLiteDatabase) {
        sqLiteDatabase.execSQL(createTableRequest(ExersizeType.PUSH_UP));
        sqLiteDatabase.execSQL(createTableRequest(ExersizeType.CURL));
    }

    @Override
    public void onUpgrade(SQLiteDatabase sqLiteDatabase, int i, int i1) {
        switch (i) {
            case 1:
                // создаем таблицу для упражнения на бицепс
                sqLiteDatabase.execSQL(createTableRequest(ExersizeType.CURL));
            case 2:
                // добавляем в таблицу поле для гугл фит о статусе синхронизации
                sqLiteDatabase.execSQL(addNewIntegerCollumnRequest(ExersizeType.PUSH_UP, GOOGLE_FIT_STATUS_COLLUMN,INTEGER_TYPE, GOOGLE_FIT_DEFAULT_VALUE));
                sqLiteDatabase.execSQL(addNewIntegerCollumnRequest(ExersizeType.CURL, GOOGLE_FIT_STATUS_COLLUMN, INTEGER_TYPE, GOOGLE_FIT_DEFAULT_VALUE));
                break;
            case 3:
                // добавляем в таблицу поле для стартДейт
                sqLiteDatabase.execSQL(addNewIntegerCollumnRequest(ExersizeType.PUSH_UP, PUSH_UP_STARTDATE_COLUMN,INTEGER_TYPE, START_DATE_DEFAULT_VALUE));
                sqLiteDatabase.execSQL(addNewIntegerCollumnRequest(ExersizeType.CURL, PUSH_UP_STARTDATE_COLUMN, INTEGER_TYPE, START_DATE_DEFAULT_VALUE));
                fillStartDateValues(sqLiteDatabase, ExersizeType.PUSH_UP);
                fillStartDateValues(sqLiteDatabase, ExersizeType.CURL);
                break;
            default:
                throw new IllegalArgumentException("bd version is not supported");
        }
    }

    private String createTableRequest(ExersizeType type) {
        return String.format(CREATE_TABLE_REQUEST, sMap.get(type));
    }

    public String selectAllStrings(ExersizeType type) {
        return String.format(SELECT_ALL_STRINGS, sMap.get(type));
    }

    public String selectMaxExersizeSet(ExersizeType type) {
        return String.format(SELECT_MAX_PUSH_UP_EXERSIZE_SET, sMap.get(type));
    }

    public String selectSumOfAllExercizes(ExersizeType type) {
        return String.format(SELECT_SUM_OF_ALL_PUSHES, sMap.get(type));
    }

    public String dropTable(ExersizeType type) {
        return String.format(DROP_PUSH_UP_TABLE, sMap.get(type));
    }

    public String selectMontSumOfAllExersizes(ExersizeType type) {
        return String.format(SELECT_MONT_SUM_OF_ALL_PUSHES, sMap.get(type));
    }

    public String selectAllStringsOrderedByTime(ExersizeType type) {
        return String.format(SELECT_ALL_STRINGS_ORDERED_BY_TIME, sMap.get(type));
    }

    public String selectStringsOrderedByTime(ExersizeType type, int firstPos, int lastPos) {
        return String.format(Locale.ENGLISH, SELECT_STRINGS_ORDERED_BY_TIME, sMap.get(type), lastPos - firstPos, firstPos);
    }

    public String getTableNameByExerciseType(ExersizeType type) {
        return sMap.get(type);
    }

    public String addNewIntegerCollumnRequest(ExersizeType type, String collumnName, String collumnType, String defaultValue){
        return String.format(ADD_GOOGLE_FIT_STATUS_COLLUMN, sMap.get(type), collumnName, collumnType, defaultValue);
    }

    public String updateCollumnWithNewValue(ExersizeType type, String collumnName, String newValue, String id){
        return String.format(UPDATE_COLLUMN_WITH_NEW_VALUE, sMap.get(type), collumnName, newValue, id);
    }

    public void fillStartDateValues(SQLiteDatabase db, ExersizeType type){
        Cursor cursor = db.rawQuery(selectAllStrings(type),null);
        while (cursor.moveToNext()){
            long id = cursor.getLong(cursor.getColumnIndex(PushUpDBOpenHelper.ID_COLUMN));
            long stopTime = cursor.getLong(cursor.getColumnIndex(PushUpDBOpenHelper.PUSH_UP_FINISH_DATE_COLUMN));
            long count = cursor.getLong(cursor.getColumnIndex(PushUpDBOpenHelper.PUSH_UP_COUNT_COLUMN));
            long startTime = stopTime - count*TIME_BETWEEN_PUSH_UP;
            db.execSQL(updateCollumnWithNewValue(type,PUSH_UP_STARTDATE_COLUMN,String.valueOf(startTime),String.valueOf(id)));
        }
        cursor.close();
    }
}
