package com.example.pushApp.async;

import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.fitness.Fitness;
import com.google.android.gms.fitness.data.DataSet;

import java.util.concurrent.Callable;

/**
 * Created by Петр on 31.05.2017.
 */

public class FitnessWriteDataCallable implements Callable<Status> {

    private GoogleApiClient mClient;
    private DataSet mDataSet;

    public FitnessWriteDataCallable(GoogleApiClient client, DataSet dataSet) {
        mClient = client;
        mDataSet = dataSet;
    }

    @Override
    public Status call() throws Exception {
        return   Fitness.HistoryApi.insertData(mClient, mDataSet).await();
    }
}
