package com.example.pushApp.async;

import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.fitness.Fitness;
import com.google.android.gms.fitness.request.DataReadRequest;
import com.google.android.gms.fitness.result.DataReadResult;

import java.util.concurrent.Callable;

/**
 * Created by Петр on 31.05.2017.
 */

public class FitnessReadDataCallable implements Callable<DataReadResult> {

    private GoogleApiClient mClient;
    DataReadRequest mRequest;

    public FitnessReadDataCallable(GoogleApiClient client, DataReadRequest request) {
        mClient = client;
        mRequest = request;
    }

    @Override
    public DataReadResult call() throws Exception {
        return Fitness.HistoryApi.readData(mClient, mRequest).await();
    }
}
