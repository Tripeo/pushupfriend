package com.example.pushApp.async;

import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.fitness.request.DataDeleteRequest;

import java.util.concurrent.Callable;

/**
 * Created by Петр on 04.06.2017.
 */

public class FitnessDeleteDataCallable implements Callable<Status> {

    private GoogleApiClient mClient;
    private DataDeleteRequest mRequest;

    public FitnessDeleteDataCallable(GoogleApiClient client, DataDeleteRequest request) {
        mClient = client;
        mRequest = request;
    }

    @Override
    public Status call() throws Exception {
        return null;
    }
}
