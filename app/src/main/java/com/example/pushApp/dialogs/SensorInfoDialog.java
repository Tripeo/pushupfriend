package com.example.pushApp.dialogs;

import android.app.Dialog;
import android.app.DialogFragment;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.Button;
import android.widget.TextView;

import com.example.pushApp.PushAppApplication;
import com.example.pushApp.R;
import com.example.pushApp.managers.interfaces.IPreferenceManager;
import com.example.pushApp.managers.implementations.PreferenceManager;

import javax.inject.Inject;

/**
 * Created by Петр on 28.01.2017.
 */

public class SensorInfoDialog extends DialogFragment implements View.OnClickListener {

    private Button backButton;
    private boolean showAdditionalInfo;
    private TextView mAdditionalInfoTextView;

    @Inject
    IPreferenceManager mPreferenceManager;

    public static SensorInfoDialog newInstance(boolean showAdditionalInfo){
        SensorInfoDialog dialog = new SensorInfoDialog();
        dialog.showAdditionalInfo = showAdditionalInfo;
        return dialog;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        ((PushAppApplication)getActivity().getApplication()).getPushAppComponent().inject(this);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View dialogView = inflater.inflate(R.layout.dialog_sensor_info_layout,container,false);

        backButton = (Button) dialogView.findViewById(R.id.back_button);
        backButton.setOnClickListener(this);
        dialogView.setOnClickListener(this);

        mAdditionalInfoTextView = (TextView) dialogView.findViewById(R.id.additional_info_text_view);

        if(!showAdditionalInfo){
            mAdditionalInfoTextView.setVisibility(View.GONE);
        }

        return dialogView;
    }

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        Dialog dialog = super.onCreateDialog(savedInstanceState);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setCanceledOnTouchOutside(true);

        return dialog;
    }

    @Override
    public void onDismiss(DialogInterface dialog) {
        super.onDismiss(dialog);
        mPreferenceManager.writeBoolean(PreferenceManager.MANUAL_MUST_SHOWN_KEY,false);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.back_button:
                dismiss();
                break;
        }
    }
}
