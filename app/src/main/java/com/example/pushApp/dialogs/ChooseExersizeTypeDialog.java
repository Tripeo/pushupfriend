package com.example.pushApp.dialogs;

import android.app.DialogFragment;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RadioGroup;

import com.example.pushApp.presentation.views.implementations.MainActivity;
import com.example.pushApp.PushAppApplication;
import com.example.pushApp.R;
import com.example.pushApp.entities.ExersizeType;
import com.example.pushApp.managers.interfaces.IPreferenceManager;

import javax.inject.Inject;

/**
 * Created by Петр on 23.02.2017.
 */

public class ChooseExersizeTypeDialog extends DialogFragment {

    private RadioGroup mRadioGroup;
    @Inject
    IPreferenceManager mPreferenceManager;


    public static ChooseExersizeTypeDialog newInstance() {

        Bundle args = new Bundle();

        ChooseExersizeTypeDialog fragment = new ChooseExersizeTypeDialog();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        ((PushAppApplication)getActivity().getApplication()).getPushAppComponent().inject(this);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View dialogView = LayoutInflater.from(getActivity()).inflate(R.layout.dialog_choose_exersize_type,container,false);
        mRadioGroup = (RadioGroup) dialogView.findViewById(R.id.radio_group);

        return dialogView;
    }

    @Override
    public void onDismiss(DialogInterface dialog) {
        super.onDismiss(dialog);
        Intent intent;
        switch (mRadioGroup.getCheckedRadioButtonId()){
            case R.id.push_radio_button:
                mPreferenceManager.writeExersizeType(ExersizeType.PUSH_UP);
                intent = new Intent(getActivity(),MainActivity.class);
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(intent);
                break;
            case R.id.curl_radio_button:
                mPreferenceManager.writeExersizeType(ExersizeType.CURL);
                intent = new Intent(getActivity(),MainActivity.class);
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(intent);
                break;
            default:
                break;
        }
    }
}
