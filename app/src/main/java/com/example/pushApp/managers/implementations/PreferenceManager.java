package com.example.pushApp.managers.implementations;

import android.content.Context;
import android.content.SharedPreferences;

import com.example.pushApp.entities.ExersizeType;
import com.example.pushApp.managers.interfaces.IPreferenceManager;

/**
 * @author Trikhin P O
 * @since 04.02.2017.
 */

public class PreferenceManager implements IPreferenceManager {

    public static final String TOP_TRESHOLD_PREFERENCE_KEY = "top_treshold_key";
    public static final int DEFAULT_TOP_TRESHOLD_VALUE = 5;
    public static final String BOTTOM_TRESHOLD_PREFERENCE_KEY = "bottom_treshold_key";
    public static final String CURRENT_EXERSIZE_TYPE_PREFERENCE_KEY = "current_exersize_key";

    public static final String MANUAL_MUST_SHOWN_KEY = "manual_must_shown_key";

    private static final String COMMON_PREFS = "common_preferences";

    private SharedPreferences prefs;

    public PreferenceManager(Context context) {
        if (prefs == null) {
            prefs = context.getSharedPreferences(COMMON_PREFS, Context.MODE_PRIVATE);
        }
    }

    @Override
    public void writeInt(String key, int value) {
        SharedPreferences.Editor editor = prefs.edit();
        editor.putInt(key, value);
        editor.apply();
    }

    @Override
    public int readInt(String key, int defaultValue) {
        return prefs.getInt(key, defaultValue);
    }

    @Override
    public void writeBoolean(String key, boolean value) {
        SharedPreferences.Editor editor = prefs.edit();
        editor.putBoolean(key, value);
        editor.apply();
    }

    @Override
    public boolean readBoolean(String key, boolean defaultValue) {
        return prefs.getBoolean(key, defaultValue);
    }

    @Override
    public void writeString(String key, String value) {
        SharedPreferences.Editor editor = prefs.edit();
        editor.putString(key, value);
        editor.apply();
    }

    @Override
    public String readString(String key, String defaultValue) {
        return prefs.getString(key, defaultValue);
    }

    @Override
    public void writeExersizeType(ExersizeType type) {
        SharedPreferences.Editor editor = prefs.edit();
        editor.putString(CURRENT_EXERSIZE_TYPE_PREFERENCE_KEY, type.name());
        editor.apply();
    }

    @Override
    public void removePreference(String key) {
        SharedPreferences.Editor editor = prefs.edit();
        editor.remove(key);
        editor.apply();
    }

    @Override
    public ExersizeType readExersizeType() {
        String typeAsString = prefs.getString(CURRENT_EXERSIZE_TYPE_PREFERENCE_KEY, ExersizeType.getDefaultType().name());
        return ExersizeType.getTypeByString(typeAsString);
    }
}
