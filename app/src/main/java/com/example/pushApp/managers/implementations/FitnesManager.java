package com.example.pushApp.managers.implementations;

import android.app.Activity;
import android.content.Context;
import android.support.v4.app.FragmentActivity;
import android.util.Log;

import com.example.pushApp.async.FitnessDeleteDataCallable;
import com.example.pushApp.async.FitnessReadDataCallable;
import com.example.pushApp.async.FitnessWriteDataCallable;
import com.example.pushApp.entities.ExersizeSet;
import com.example.pushApp.entities.ExersizeType;
import com.example.pushApp.managers.interfaces.IFitnessManager;
import com.example.pushApp.rx.SimpleObserver;
import com.google.android.gms.auth.api.signin.GoogleSignIn;
import com.google.android.gms.common.Scopes;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.Scope;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.fitness.FitnessOptions;
import com.google.android.gms.fitness.data.DataPoint;
import com.google.android.gms.fitness.data.DataSet;
import com.google.android.gms.fitness.data.DataSource;
import com.google.android.gms.fitness.data.DataType;
import com.google.android.gms.fitness.data.Field;
import com.google.android.gms.fitness.data.WorkoutExercises;
import com.google.android.gms.fitness.request.DataDeleteRequest;
import com.google.android.gms.fitness.request.DataReadRequest;
import com.google.android.gms.fitness.result.DataReadResult;

import java.util.List;
import java.util.concurrent.TimeUnit;

import io.reactivex.Observable;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;

/**
 * @author Trikhin P O
 */

public class FitnesManager implements IFitnessManager {

    public static final String TAG = "FitnesManager";

    private static final int GOOGLE_FIT_SYNC_PORTION_SIZE = 100;

    private GoogleApiClient mClient = null;
    private Context mContext;

    public FitnesManager(Context context) {
        mContext = context;
    }

    @Override
    public void buildFitnessClient(Activity activity) {

    }

    @Override
    public void writeData(Context context, int count, long startTime, long endTime) {

        DataSource dataSource = new DataSource.Builder().
                setDataType(DataType.TYPE_WORKOUT_EXERCISE)
                .setAppPackageName(context)
                .setType(DataSource.TYPE_DERIVED).build();

        final DataSet dataSet = DataSet.create(dataSource);
        DataPoint dataPoint = dataSet.createDataPoint();
        dataPoint.setTimeInterval(startTime, endTime, TimeUnit.MILLISECONDS);
        dataPoint.getValue(Field.FIELD_EXERCISE).setString(WorkoutExercises.PUSHUP);
        dataPoint.getValue(Field.FIELD_REPETITIONS).setInt(count);
        dataSet.add(dataPoint);

        Observable.fromCallable(new FitnessWriteDataCallable(mClient,dataSet)).
                subscribeOn(Schedulers.newThread()).
                observeOn(AndroidSchedulers.mainThread())
                .subscribe(new SimpleObserver<Status>() {
                    @Override
                    public void onNext(Status value) {
                        if (!value.isSuccess()){
                            Log.e(TAG,"Delete request failed, status is " + value.getStatus().toString());
                        }
                    }
                });
    }

    @Override
    public void readData(Context context, long startTime, long endTime) {

        final DataReadRequest readRequest = new DataReadRequest.Builder()
                .read(DataType.TYPE_WORKOUT_EXERCISE)
                .setTimeRange(startTime, endTime, TimeUnit.MILLISECONDS)
                .build();

        Observable.fromCallable(new FitnessReadDataCallable(mClient,readRequest))
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new SimpleObserver<DataReadResult>() {
                    @Override
                    public void onNext(DataReadResult value) {
                        if (value.getDataSet(DataType.TYPE_WORKOUT_EXERCISE).getDataPoints().size()>0) {
                            Log.d(TAG, value.getDataSet(DataType.TYPE_WORKOUT_EXERCISE).getDataPoints().get(0).toString());
                        } else {
                            Log.d(TAG,"array is empty");
                        }
                    }
                });
    }

    @Override
    public void deleteData(Context context, long startTime, long endTime) {

        final DataDeleteRequest deleteRequest = new DataDeleteRequest.Builder().setTimeInterval(startTime, endTime, TimeUnit.MILLISECONDS)
                .addDataType(DataType.TYPE_WORKOUT_EXERCISE)
                .build();

        Observable.fromCallable(new FitnessDeleteDataCallable(mClient,deleteRequest))
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new SimpleObserver<Status>() {
                    @Override
                    public void onNext(Status value) {
                        if (!value.isSuccess()){
                            Log.e(TAG,"Delete request failed, status is " + value.getStatus().toString());
                        }
                    }
                });
    }

    @Override
    public void syncPortionExersizeSets(int setsCount, ExersizeSetManager exersizeSetManager) {
        List<ExersizeSet> sets = exersizeSetManager.getNonSyncExersizeSetsOrderedByTime(ExersizeType.PUSH_UP,0,GOOGLE_FIT_SYNC_PORTION_SIZE);

    }
}
