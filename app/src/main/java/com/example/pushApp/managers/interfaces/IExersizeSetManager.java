package com.example.pushApp.managers.interfaces;

import android.support.annotation.Nullable;

import com.example.pushApp.entities.ExersizeSet;
import com.example.pushApp.entities.ExersizeType;

import java.util.List;

/**
 * @author Trikhin P O
 */

public interface IExersizeSetManager {

    void writeExersizeSet(ExersizeSet set);

    String readAllDatabase(ExersizeType type);

    String prettyPrintExercizeSets(ExersizeType type);

    List<ExersizeSet> getAllExersizeSets(ExersizeType type);

    List<ExersizeSet> getNonSyncExersizeSetsOrderedByTime(ExersizeType type, int startPosition, int stopPosition);

    List<ExersizeSet> getDayExersizeSets(ExersizeType type, long time);

    List<ExersizeSet> getMaximumDayExersizeSet(ExersizeType type);

    @Nullable
    ExersizeSet getAbsolutelyMaximumSet(ExersizeType type);

    long getTotalAmountExersizes(ExersizeType type);

    long getMonthTotalAmountExersizes(ExersizeType type);

    List<ExersizeSet> getBestMonthExersizeSet(ExersizeType type);

    void dropExersizeTypeTable(ExersizeType type);

    void closeDatabase();

    float getSensorMaxValue();

    void setSensorMaxValue(float maxValue);

    void writeDatabaseOnDisk(ExersizeType exersizeType);

    void loadDatabaseFromDisk(ExersizeType exersizeType);
}
