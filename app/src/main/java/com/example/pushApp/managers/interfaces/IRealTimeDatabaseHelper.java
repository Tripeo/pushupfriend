package com.example.pushApp.managers.interfaces;

/**
 * Интерфейс описывающий работу автосинхронизующейся базы данных.
 *
 * @author Trikhin P O
 * @since 10.03.2018
 */

public interface IRealTimeDatabaseHelper {

    public void writePushUpSet(int pushUpCount);

}
