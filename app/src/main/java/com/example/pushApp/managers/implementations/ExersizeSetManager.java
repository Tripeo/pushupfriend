package com.example.pushApp.managers.implementations;

import android.content.ContentValues;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Environment;
import android.support.annotation.Nullable;

import com.example.pushApp.databases.PushUpDBOpenHelper;
import com.example.pushApp.entities.ExersizeSet;
import com.example.pushApp.entities.ExersizeType;
import com.example.pushApp.managers.interfaces.IExersizeSetManager;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.List;

/**
 * Created by Петр on 07.11.2016.
 */

public class ExersizeSetManager implements IExersizeSetManager {

    public static final String DATABASE_EXCHANGE_FILE = "/pushAppDb.txt";

    public static final String DATE_FORMAT = "dd MMM yyyy";

    private PushUpDBOpenHelper mPushUpDBOpenHelper;

    private float mSensorMaxValue;

    public ExersizeSetManager(PushUpDBOpenHelper pushUpDBOpenHelper) {
        mPushUpDBOpenHelper = pushUpDBOpenHelper;
    }

    @Override
    public void writeExersizeSet(ExersizeSet set) {
        SQLiteDatabase db = mPushUpDBOpenHelper.getWritableDatabase();
        ContentValues contentValues = new ContentValues();
        contentValues.put(PushUpDBOpenHelper.PUSH_UP_COUNT_COLUMN, set.getCount());
        contentValues.put(PushUpDBOpenHelper.PUSH_UP_FINISH_DATE_COLUMN, set.getEndDate());
        db.insert(mPushUpDBOpenHelper.getTableNameByExerciseType(set.getType()), null, contentValues);
        db.close();
    }

    @Override
    public String readAllDatabase(ExersizeType type) {
        SQLiteDatabase db = mPushUpDBOpenHelper.getWritableDatabase();
        Cursor cursor = db.rawQuery(mPushUpDBOpenHelper.selectAllStrings(type), null);
        String result = cursorToString(cursor);
        db.close();
        return result;
    }

    @Override
    public String prettyPrintExercizeSets(ExersizeType type) {
        SQLiteDatabase db = mPushUpDBOpenHelper.getWritableDatabase();
        Cursor cursor = db.rawQuery(mPushUpDBOpenHelper.selectAllStrings(type), null);
        String result = exersizeSetCursorPrettyPrint(cursor);
        db.close();
        return result;
    }

    @Override
    public List<ExersizeSet> getAllExersizeSets(ExersizeType type) {
        SQLiteDatabase db = mPushUpDBOpenHelper.getWritableDatabase();
        List<ExersizeSet> result = new ArrayList<>();
        Cursor cursor = db.rawQuery(mPushUpDBOpenHelper.selectAllStrings(type), null);
        if (cursor != null) {
            if (cursor.moveToFirst()) {
                result.add(buildSetFromCursor(type,cursor));
                while (cursor.moveToNext()) {
                    result.add(buildSetFromCursor(type,cursor));
                }
            }
            cursor.close();
        }
        db.close();
        return result;
    }

    @Override
    public List<ExersizeSet> getNonSyncExersizeSetsOrderedByTime(ExersizeType type, int startPosition, int stopPosition) {
        SQLiteDatabase db = mPushUpDBOpenHelper.getWritableDatabase();
        List<ExersizeSet> result = new ArrayList<>();
        Cursor cursor = db.rawQuery(mPushUpDBOpenHelper.selectStringsOrderedByTime(type, startPosition, stopPosition), null);
        if (cursor != null) {
            if (cursor.moveToFirst()) {
                result.add(buildSetFromCursor(type,cursor));
                while (cursor.moveToNext()) {
                    result.add(buildSetFromCursor(type,cursor));
                }
            }
            cursor.close();
        }
        db.close();
        return result;
    }

    @Override
    public List<ExersizeSet> getDayExersizeSets(ExersizeType type, long time) {
        SQLiteDatabase db = mPushUpDBOpenHelper.getWritableDatabase();
        List<ExersizeSet> result = new ArrayList<>();
        Cursor cursor = db.rawQuery(mPushUpDBOpenHelper.selectAllStrings(type), null);
        if (cursor != null) {

            Calendar tempCalendar = new GregorianCalendar();
            tempCalendar.setTimeInMillis(time);
            tempCalendar.set(Calendar.HOUR_OF_DAY, 0);
            tempCalendar.set(Calendar.MINUTE, 0);
            tempCalendar.set(Calendar.SECOND, 0);
            tempCalendar.set(Calendar.MILLISECOND, 0);

            long bottomTime = tempCalendar.getTimeInMillis();
            tempCalendar.add(Calendar.DAY_OF_YEAR, 1);
            long topTime = tempCalendar.getTimeInMillis();

            if (cursor.moveToFirst()) {
                do {
                    if (cursor.getLong(cursor.getColumnIndex(PushUpDBOpenHelper.PUSH_UP_FINISH_DATE_COLUMN)) >= bottomTime &&
                            cursor.getLong(cursor.getColumnIndex(PushUpDBOpenHelper.PUSH_UP_FINISH_DATE_COLUMN)) < topTime) {
                        result.add(buildSetFromCursor(type,cursor));
                    }
                } while (cursor.moveToNext());
            }
            cursor.close();
        }
        db.close();
        return result;
    }

    @Override
    public List<ExersizeSet> getMaximumDayExersizeSet(ExersizeType type) {
        SQLiteDatabase db = mPushUpDBOpenHelper.getWritableDatabase();
        Cursor cursor = db.rawQuery(mPushUpDBOpenHelper.selectAllStringsOrderedByTime(type), null);

        Calendar tempCalendar = new GregorianCalendar();
        long bottomTime = 0;
        long currentTime = 0;
        List<ExersizeSet> currentSet = new ArrayList<>();
        List<ExersizeSet> result = new ArrayList<>();
        if (cursor != null) {
            if (cursor.moveToFirst()) {
                tempCalendar.setTimeInMillis(cursor.getLong(cursor.getColumnIndex(PushUpDBOpenHelper.PUSH_UP_FINISH_DATE_COLUMN)));
                tempCalendar.set(Calendar.HOUR_OF_DAY, 0);
                tempCalendar.set(Calendar.MINUTE, 0);
                tempCalendar.set(Calendar.SECOND, 0);
                tempCalendar.set(Calendar.MILLISECOND, 0);
                bottomTime = tempCalendar.getTimeInMillis();
                do {
                    currentTime = cursor.getLong(cursor.getColumnIndex(PushUpDBOpenHelper.PUSH_UP_FINISH_DATE_COLUMN));
                    if (currentTime < bottomTime) {
                        if (sumSetCount(result) < sumSetCount(currentSet)) {
                            result = currentSet;
                        }
                        tempCalendar.setTimeInMillis(currentTime);
                        tempCalendar.set(Calendar.HOUR_OF_DAY, 0);
                        tempCalendar.set(Calendar.MINUTE, 0);
                        tempCalendar.set(Calendar.SECOND, 0);
                        tempCalendar.set(Calendar.MILLISECOND, 0);
                        bottomTime = tempCalendar.getTimeInMillis();
                        currentSet = new ArrayList<>();
                        currentSet.add(buildSetFromCursor(type,cursor));
                    } else {
                        currentSet.add(buildSetFromCursor(type,cursor));
                    }
                    tempCalendar.setTimeInMillis(cursor.getLong(cursor.getColumnIndex(PushUpDBOpenHelper.PUSH_UP_FINISH_DATE_COLUMN)));
                } while (cursor.moveToNext());
                //Это нужно чтоб проверить самый первый день (потому что для него currentTime никогда не будет меньше чем bottomTime)
                if (sumSetCount(result) < sumSetCount(currentSet)) {
                    result = currentSet;
                }
            }
            cursor.close();
        }
        db.close();

        return result;
    }

    @Override
    @Nullable
    public ExersizeSet getAbsolutelyMaximumSet(ExersizeType type) {
        ExersizeSet resultSet = null;
        SQLiteDatabase db = mPushUpDBOpenHelper.getWritableDatabase();
        Cursor cursor = db.rawQuery(mPushUpDBOpenHelper.selectMaxExersizeSet(type), null);
        if (cursor != null) {
            if (cursor.moveToFirst()) {
                resultSet = buildSetFromCursor(type,cursor);
            }
            cursor.close();
        }
        db.close();
        return resultSet;
    }

    @Override
    public void dropExersizeTypeTable(ExersizeType type) {
        SQLiteDatabase db = mPushUpDBOpenHelper.getWritableDatabase();
        db.execSQL(mPushUpDBOpenHelper.dropTable(type));
        db.close();
    }

    @Override
    public long getTotalAmountExersizes(ExersizeType type) {
        long result = 0;
        SQLiteDatabase db = mPushUpDBOpenHelper.getWritableDatabase();
        Cursor cursor = db.rawQuery(mPushUpDBOpenHelper.selectSumOfAllExercizes(type), null);
        if (cursor != null) {
            if (cursor.moveToFirst()) {
                result = cursor.getLong(0);
            }
            cursor.close();
        }
        db.close();
        return result;
    }

    @Override
    public long getMonthTotalAmountExersizes(ExersizeType type) {
        Calendar calendar = new GregorianCalendar();
        calendar.set(Calendar.DAY_OF_MONTH, 1);
        calendar.set(Calendar.HOUR_OF_DAY, 0);
        calendar.set(Calendar.MINUTE, 0);
        calendar.set(Calendar.SECOND, 0);
        long beginningOfMonthTme = calendar.getTimeInMillis();
        long resultPushUpCount = 0;
        SQLiteDatabase db = mPushUpDBOpenHelper.getWritableDatabase();
        Cursor cursor = db.rawQuery(mPushUpDBOpenHelper.selectMontSumOfAllExersizes(type), new String[]{beginningOfMonthTme + ""});
        if (cursor != null) {
            if (cursor.moveToFirst()) {
                resultPushUpCount = cursor.getLong(0);
            }
            cursor.close();
        }
        db.close();
        return resultPushUpCount;
    }

    @Override
    public List<ExersizeSet> getBestMonthExersizeSet(ExersizeType type) {
        SQLiteDatabase db = mPushUpDBOpenHelper.getWritableDatabase();
        Cursor cursor = db.rawQuery(mPushUpDBOpenHelper.selectAllStringsOrderedByTime(type), null);

        Calendar tempCalendar = new GregorianCalendar();
        long bottomTime = 0;
        long currentTime = 0;
        List<ExersizeSet> currentSet = new ArrayList<>();
        List<ExersizeSet> result = new ArrayList<>();
        if (cursor != null) {
            if (cursor.moveToFirst()) {
                tempCalendar.setTimeInMillis(cursor.getLong(cursor.getColumnIndex(PushUpDBOpenHelper.PUSH_UP_FINISH_DATE_COLUMN)));
                tempCalendar.set(Calendar.DAY_OF_MONTH, 1);
                tempCalendar.set(Calendar.HOUR_OF_DAY, 0);
                tempCalendar.set(Calendar.SECOND, 0);
                tempCalendar.set(Calendar.MILLISECOND, 0);
                bottomTime = tempCalendar.getTimeInMillis();
                do {
                    currentTime = cursor.getLong(cursor.getColumnIndex(PushUpDBOpenHelper.PUSH_UP_FINISH_DATE_COLUMN));
                    if (currentTime < bottomTime) {
                        if (sumSetCount(result) < sumSetCount(currentSet)) {
                            result = currentSet;
                        }
                        tempCalendar.setTimeInMillis(currentTime);
                        tempCalendar.set(Calendar.DAY_OF_MONTH, 1);
                        tempCalendar.set(Calendar.HOUR_OF_DAY, 0);
                        tempCalendar.set(Calendar.SECOND, 0);
                        tempCalendar.set(Calendar.MILLISECOND, 0);
                        bottomTime = tempCalendar.getTimeInMillis();
                        currentSet = new ArrayList<>();
                        currentSet.add(buildSetFromCursor(type,cursor));
                    } else {
                        currentSet.add(buildSetFromCursor(type,cursor));
                    }
                    tempCalendar.setTimeInMillis(cursor.getLong(cursor.getColumnIndex(PushUpDBOpenHelper.PUSH_UP_FINISH_DATE_COLUMN)));
                } while (cursor.moveToNext());
                //Это нужно чтоб проверить самый первый день (потому что для него currentTime никогда не будет меньше чем bottomTime)
                if (sumSetCount(result) < sumSetCount(currentSet)) {
                    result = currentSet;
                }
            }
            cursor.close();
        }
        db.close();

        return result;
    }

    @Override
    public void closeDatabase() {
        mPushUpDBOpenHelper.close();
    }

    @Override
    public float getSensorMaxValue() {
        return mSensorMaxValue;
    }

    @Override
    public void setSensorMaxValue(float maxValue) {
        mSensorMaxValue = maxValue;
    }

    private int sumSetCount(List<ExersizeSet> exersizeSets) {
        int result = 0;
        for (ExersizeSet set : exersizeSets) {
            result = result + set.getCount();
        }
        return result;
    }

    @Override
    public void writeDatabaseOnDisk(ExersizeType type) {
        try {
            File file = new File(Environment.getExternalStorageDirectory().getAbsolutePath() + DATABASE_EXCHANGE_FILE);
            FileWriter fileWriter = new FileWriter(file);
            List<ExersizeSet> exersizeSets = getAllExersizeSets(type);
            JSONArray setsAsJson = new JSONArray();
            JSONObject tempJsonObj = new JSONObject();
            for (ExersizeSet set : exersizeSets) {
                tempJsonObj = new JSONObject();
                tempJsonObj.put(PushUpDBOpenHelper.PUSH_UP_COUNT_COLUMN, set.getCount());
                tempJsonObj.put(PushUpDBOpenHelper.PUSH_UP_FINISH_DATE_COLUMN, set.getEndDate());
                setsAsJson.put(tempJsonObj);
            }
            fileWriter.write(setsAsJson.toString());
            fileWriter.close();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void loadDatabaseFromDisk(ExersizeType type) {
        try {
            File file = new File(Environment.getExternalStorageDirectory().getAbsolutePath() + DATABASE_EXCHANGE_FILE);
            FileReader fileReader = new FileReader(file);
            BufferedReader bufferedReader = new BufferedReader(fileReader);
            String tempString = "";
            String fullString = "";
            while ((tempString = bufferedReader.readLine()) != null) {
                fullString = fullString + tempString;
            }
            JSONArray array = new JSONArray(fullString);
            for (int i = 0; i < array.length(); i++) {
                ExersizeSet set = new ExersizeSet(type,ExersizeSet.DEFAULT_ID,
                        array.getJSONObject(i).getInt(PushUpDBOpenHelper.PUSH_UP_COUNT_COLUMN),
                        array.getJSONObject(i).getLong(PushUpDBOpenHelper.PUSH_UP_FINISH_DATE_COLUMN));
                writeExersizeSet(set);
            }
            fileReader.close();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    private String cursorToString(Cursor cursor) {
        StringBuilder stringBuilder = new StringBuilder();
        if (cursor != null && cursor.moveToFirst()) {
            do {
                for (int i = 0; i < cursor.getColumnCount(); i++) {
                    stringBuilder.append(cursor.getColumnName(i)).append(" ").append(cursor.getString(cursor.getColumnIndex(cursor.getColumnName(i)))).append("; ");
                }
                stringBuilder.append("\n");
            } while (cursor.moveToNext());
        }
        return stringBuilder.toString();
    }

    private String exersizeSetCursorPrettyPrint(Cursor cursor) {

        StringBuilder stringBuilder = new StringBuilder();
        if (cursor != null && cursor.moveToFirst()) {
            do {
                for (String collumnName : cursor.getColumnNames()){
                    stringBuilder.append(collumnName+ " =").append(cursor.getString((cursor.getColumnIndex(collumnName)))).append("; ");
                }
                stringBuilder.append("\n");
            } while (cursor.moveToNext());
        }
        return stringBuilder.toString();
    }

    private ExersizeSet buildSetFromCursor(ExersizeType type, Cursor cursor){
        return new ExersizeSet(type,
                cursor.getInt(cursor.getColumnIndex(PushUpDBOpenHelper.ID_COLUMN)),
                cursor.getInt(cursor.getColumnIndex(PushUpDBOpenHelper.PUSH_UP_COUNT_COLUMN)),
                cursor.getLong(cursor.getColumnIndex(PushUpDBOpenHelper.PUSH_UP_FINISH_DATE_COLUMN)));
    }
}
