package com.example.pushApp.managers.interfaces;

/**
 * Менеджер отвечающий за данные пользователя
 * Например firebase.uid
 *
 * @author Trikhin P O
 * @since 08.03.2018
 */

public interface IUserDataManager {

    public static final String FIREBASE_UID_KEY = "firebaseUid";

    /**
     * Сохранить уникальный идентификатор пользователя
     */
    public void saveFirebaseUID (String fireBaseUid);

    /**
     * Получить уникальный идентификатор пользователя
     */
    public String getFirebaseUID();

    /**
     * Удалить уникальный идентификатор пользователя
     */
    public void cleanFirebaseUID();
}
