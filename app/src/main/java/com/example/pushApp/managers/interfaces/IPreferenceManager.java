package com.example.pushApp.managers.interfaces;

import com.example.pushApp.entities.ExersizeType;

/**
 * @author Trikhin P O
 * @since 04.02.2017.
 */

public interface IPreferenceManager {

    void writeInt (String key, int value);

    int readInt (String key, int defaultValue);

    void writeBoolean(String key, boolean value);

    boolean readBoolean (String key, boolean defaultValue);

    void writeString(String key, String value);

    String readString(String key, String defaultValue);

    void writeExersizeType(ExersizeType type);

    void removePreference(String key);

    ExersizeType readExersizeType();

}
