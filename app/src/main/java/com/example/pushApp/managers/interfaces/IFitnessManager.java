package com.example.pushApp.managers.interfaces;

import android.app.Activity;
import android.content.Context;
import android.support.v4.app.FragmentActivity;

import com.example.pushApp.managers.implementations.ExersizeSetManager;

/**
 * @author Trikhin Pete
 */

public interface IFitnessManager {

    /**
     * Подключение к GoogleFit, запрос разрешений на чтение и запись данных
     * @param activity - любая {@link Activity}
     */
    void buildFitnessClient(Activity activity);

    /**
     * Запись данных в GoogleFit
     * @param context
     * @param count количество подтягиваний
     * @param startTime начало упражнения
     * @param endTime конец упражнения
     */
    void writeData(Context context, int count, long startTime, long endTime);

    /**
     * Чтение данных из GoogleFit
     * @param context
     * @param startTime начало промежутка времени
     * @param endTime конец промежутка
     */
    void readData(Context context, long startTime, long endTime);

    /**
     * Удаление данных из GoogleFit
     * @param context
     * @param startTime начало промежутка
     * @param endTime конец промежукта
     */
    void deleteData(Context context, long startTime, long endTime);


    void syncPortionExersizeSets(int setsCount, ExersizeSetManager exersizeSetManager);
}
