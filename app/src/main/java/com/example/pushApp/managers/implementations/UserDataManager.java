package com.example.pushApp.managers.implementations;

import com.example.pushApp.managers.interfaces.IPreferenceManager;
import com.example.pushApp.managers.interfaces.IUserDataManager;

/**
 * Дефолтная реализация {@link com.example.pushApp.managers.interfaces.IUserDataManager}
 *
 * @author Trikhin P O
 * @since 08.03.2018
 */

public class UserDataManager implements IUserDataManager {

    IPreferenceManager mPreferenceManager;

    public UserDataManager(IPreferenceManager preferenceManager) {
        mPreferenceManager = preferenceManager;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void saveFirebaseUID(String fireBaseUid) {
        mPreferenceManager.writeString(FIREBASE_UID_KEY, fireBaseUid);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public String getFirebaseUID() {
        return mPreferenceManager.readString(FIREBASE_UID_KEY, null);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void cleanFirebaseUID() {
        mPreferenceManager.removePreference(FIREBASE_UID_KEY);
    }
}
