package com.example.pushApp.customviews.adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.example.pushApp.R;
import com.example.pushApp.entities.ExersizeSet;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Locale;

/**
 * Created by Петр on 10.01.2017.
 */

public class DetailExersizeListAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private static final int TYTLE_ITEM_TYPE = 1;
    private static final int EXERSIZE_ITEM_TYPE = 2;
    private static String DATE_FORMAT = "dd MMMM yyyy";
    private static String DETAIL_EX_DATE_FORMAT = "HH:mm:ss";

    private List<CommonBinder> mData = new ArrayList<>();

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        RecyclerView.ViewHolder holder = null;

        LayoutInflater inflater = LayoutInflater.from(parent.getContext());
        View holderView;
        switch (viewType){
            case EXERSIZE_ITEM_TYPE:
                holderView = inflater.inflate(R.layout.detail_exersize_list_element,parent,false);
                holder =  new ExersizeElementViewHolder(holderView);
                break;
            case TYTLE_ITEM_TYPE:
                holderView = inflater.inflate(R.layout.title_list_element,parent,false);
                holder =  new TitleElementViewHolder(holderView);
        }
        return holder;
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        switch (getItemViewType(position)){
            case TYTLE_ITEM_TYPE:
                TitleElementViewHolder titleElementViewHolderHolder = (TitleElementViewHolder)holder;
                titleElementViewHolderHolder.bind((TitleBinder)mData.get(position));
                break;
            case EXERSIZE_ITEM_TYPE:
                ExersizeElementViewHolder exersizeElementViewHolder = (ExersizeElementViewHolder)holder;
                boolean lastInPos = (mData.size() -1) ==position;
                exersizeElementViewHolder.bind((ExersizeElementBinder) mData.get(position),lastInPos);
                break;
        }
    }

    @Override
    public int getItemViewType(int position) {
        return mData.get(position).getType();
    }

    @Override
    public int getItemCount() {
        return mData.size();
    }

    public List<CommonBinder> getData() {
        return mData;
    }

    public void setExersizeData(List<ExersizeSet> data, long time, Context context) {
        mData.clear();
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat(DATE_FORMAT,new Locale("RU"));
        if (data.size()==0){
            mData.add(new TitleBinder(simpleDateFormat.format(new Date(time)),context.getString(R.string.no_exersizes_this_day)));
        } else {
            int count = 0;
            for (ExersizeSet set : data){
                count = count+ set.getCount();
            }
            mData.add(new TitleBinder(simpleDateFormat.format(new Date(time)),"Всего повторений - " + count));
        }

        for(ExersizeSet exersizeSet : data){
            mData.add(new ExersizeElementBinder(exersizeSet));
        }

        notifyDataSetChanged();
    }

    public static class TitleElementViewHolder extends RecyclerView.ViewHolder{

        private TextView mTitleTextView;
        private TextView mCountTextView;

        public TitleElementViewHolder(View itemView) {
            super(itemView);
            mTitleTextView = (TextView) itemView.findViewById(R.id.title_text_view);
            mCountTextView = (TextView) itemView.findViewById(R.id.all_count_text_view);
        }

        public void bind(TitleBinder binder){
            mTitleTextView.setText(binder.getTitleTop());
            mCountTextView.setText(binder.getTitleBottom());
        }
    }

    public static class ExersizeElementViewHolder extends RecyclerView.ViewHolder {

        private TextView mSumTextView;
        private TextView mDateTextView;
        private SimpleDateFormat mDetailExSimpleDateFormat;
        private View mDivider;

        public ExersizeElementViewHolder(View itemView) {
            super(itemView);
            mSumTextView = (TextView) itemView.findViewById(R.id.count_text_view);
            mDateTextView = (TextView) itemView.findViewById(R.id.date_text_view);
            mDetailExSimpleDateFormat = new SimpleDateFormat(DetailExersizeListAdapter.DETAIL_EX_DATE_FORMAT,Locale.ENGLISH);
            mDivider = (View) itemView.findViewById(R.id.divider);
        }

        public void bind(ExersizeElementBinder binder, boolean lastInPosition){
            mSumTextView.setText(binder.getExersizeSet().getCount()+"");
            mDateTextView.setText(mDetailExSimpleDateFormat.format(new Date(binder.getExersizeSet().getEndDate())));
            if (lastInPosition){
                mDivider.setVisibility(View.GONE);
            } else {
                mDivider.setVisibility(View.VISIBLE);
            }
        }
    }


    public static class CommonBinder{

        private int mType;

        public CommonBinder(int type) {
            mType = type;
        }

        public int getType() {
            return mType;
        }
    }

    public static class TitleBinder extends CommonBinder{

        private String mTitleTop;
        private String mTitleBottom;

        public TitleBinder(String titleTop, String mTitleBottom) {
            super(TYTLE_ITEM_TYPE);
            mTitleTop = titleTop;
            this.mTitleBottom = mTitleBottom;
        }

        public String getTitleTop() {
            return mTitleTop;
        }

        public String getTitleBottom() {
            return mTitleBottom;
        }
    }

    public static class ExersizeElementBinder extends CommonBinder{

        private ExersizeSet mExersizeSet;

        public ExersizeElementBinder(ExersizeSet set) {
            super(EXERSIZE_ITEM_TYPE);
            mExersizeSet = set;
        }

        public ExersizeSet getExersizeSet() {
            return mExersizeSet;
        }
    }


}
