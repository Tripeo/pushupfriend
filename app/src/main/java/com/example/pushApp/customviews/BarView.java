package com.example.pushApp.customviews;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.RectF;
import android.os.Build;
import android.util.AttributeSet;
import android.view.View;

import com.example.pushApp.R;
import com.example.pushApp.utils.ViewUtils;

/**
 * Created by Петр on 19.12.2016.
 */

public class BarView extends View {

    private static final int BAR_WIDTH = 6;
    private static final int BAR_ROUND_RADIUS_X = 3;
    private static final int BAR_ROUND_RADIUS_Y = 3;
    private static final int BAR_STROKE_WIDTH = 3;


    private float mHeight;
    private Paint mPaint;
    private Paint mBorderPaint;
    private boolean isSelected;

    //Используется в onDraw - вынесено чтоб не перевыделять память
    private RectF mRect = new RectF();

    public BarView(Context context) {
        super(context);
        initialization();
    }

    public BarView(Context context, AttributeSet attrs) {
        super(context, attrs);
        initialization();
    }

    public BarView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        initialization();
    }

    public void setHeight(float height) {
        mHeight = height;
    }

    @Override
    protected void onDraw(Canvas canvas) {
        super.onDraw(canvas);
        mRect.set((getWidth()-BAR_WIDTH*ViewUtils.DP_COEFFICIENT)/2,
                (getHeight() - mHeight*ViewUtils.DP_COEFFICIENT),
                ((getWidth()+BAR_WIDTH*ViewUtils.DP_COEFFICIENT)/2),
                getHeight());
        canvas.drawRoundRect(mRect,
                BAR_ROUND_RADIUS_X*ViewUtils.DP_COEFFICIENT,
                BAR_ROUND_RADIUS_Y*ViewUtils.DP_COEFFICIENT,
                mPaint);
        if (isSelected){
            canvas.drawRoundRect(mRect,
            BAR_ROUND_RADIUS_X*ViewUtils.DP_COEFFICIENT,
            BAR_ROUND_RADIUS_Y*ViewUtils.DP_COEFFICIENT,
            mBorderPaint);
        }
    }

    @Override
    public boolean isSelected() {
        return isSelected;
    }

    @Override
    public void setSelected(boolean selected) {
        isSelected = selected;
        invalidate();
    }

    private void initialization (){
        mPaint = new Paint();
        if (Build.VERSION.SDK_INT>=Build.VERSION_CODES.M){
            mPaint.setColor(getContext().getColor(R.color.colorAccent));
        } else {
            mPaint.setColor(getContext().getResources().getColor(R.color.colorAccent));
        }
        mBorderPaint = new Paint();
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            mBorderPaint.setColor(getContext().getColor(R.color.colorAccent));
        } else {
            mBorderPaint.setColor(getContext().getResources().getColor(R.color.colorAccent));
        }
        mBorderPaint.setStyle(Paint.Style.STROKE);
        mBorderPaint.setStrokeWidth(BAR_STROKE_WIDTH*ViewUtils.DP_COEFFICIENT);
    }
}
