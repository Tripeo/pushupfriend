package com.example.pushApp.customviews.adapters;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Петр on 22.01.2017.
 */

public class MainPagerAdapter extends FragmentPagerAdapter {

    private List<Fragment> mData;

    public MainPagerAdapter(FragmentManager fm) {
        super(fm);
    }

    @Override
    public Fragment getItem(int position) {
        return mData.get(position);
    }

    @Override
    public int getCount() {
        return mData.size();
    }

    public void setData(List<Fragment> data) {
        mData = data;
    }

    public void addFragment(Fragment fragment){
        if(mData == null){
            mData = new ArrayList<>();
        }
        mData.add(fragment);
    }
}
