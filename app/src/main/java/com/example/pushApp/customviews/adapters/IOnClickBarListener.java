package com.example.pushApp.customviews.adapters;

import android.view.View;

/**
 * Created by Петр on 14.01.2017.
 */

public interface IOnClickBarListener {
    void onBarClicked(View view);
}
