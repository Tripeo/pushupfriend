package com.example.pushApp.customviews.adapters;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.pushApp.R;
import com.example.pushApp.customviews.BarView;
import com.example.pushApp.entities.BarEntity;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Петр on 13.12.2016.
 */

public class HistogramAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> implements View.OnClickListener {

    public static final int BAR_VIEW_TYPE = 0;
    public static final int EMPTY_VIEW_TYPE = 1;

    private List<BarEntity> mData;
    private IOnClickBarListener mOnClickBarListener;

    public HistogramAdapter() {
        mData = new ArrayList<>();
    }

    @Override
    public int getItemViewType(int position) {
        return BAR_VIEW_TYPE;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        CommonHolder holder = null;

        switch (viewType){
            case BAR_VIEW_TYPE:
                LayoutInflater inflater = LayoutInflater.from(parent.getContext());
                View view = inflater.inflate(R.layout.histogram_list_element,null);
                view.setOnClickListener(this);
                holder = new HistogramListElementBarHolder(view);
                break;
            case EMPTY_VIEW_TYPE:
                holder = new HistogramListElementEmptyHolder(new View(parent.getContext()));;
                break;
        }
        return holder;
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        switch (holder.getItemViewType()){
            case BAR_VIEW_TYPE:
                ((CommonHolder)holder).bind(holder,position);
                break;
            case EMPTY_VIEW_TYPE:
                break;
        }
    }

    @Override
    public int getItemCount() {
        return mData.size();
    }

    public void setData(List<BarEntity> data) {
        mData = data;
    }

    public void setOnClickBarListener(IOnClickBarListener onClickBarListener) {
        mOnClickBarListener = onClickBarListener;
    }

    public List<BarEntity> getData() {
        return mData;
    }

    @Override
    public void onClick(View v) {
        if (mOnClickBarListener != null ){
            mOnClickBarListener.onBarClicked(v);
        }
    }

    public class CommonHolder extends RecyclerView.ViewHolder{

        private int mViewType;

        public CommonHolder(View itemView, int viewType) {
            super(itemView);
            mViewType = viewType;
        }

        public void bind (RecyclerView.ViewHolder holder, int position){

        }

        public int getViewType() {
            return mViewType;
        }
    }

    public class HistogramListElementBarHolder extends CommonHolder {

        private BarView mBarView;
        private TextView mTextView;
        private View mVerticalLineView;
        private View mVerticalLineUnderHorizont;
        private ImageView mImageView;

        public HistogramListElementBarHolder(View itemView) {
            super(itemView,BAR_VIEW_TYPE);
            mBarView = (BarView) itemView.findViewById(R.id.bar_view);
            mTextView = (TextView) itemView.findViewById(R.id.date_text_view);
            mVerticalLineView = itemView.findViewById(R.id.vertical_line);
            mVerticalLineUnderHorizont = itemView.findViewById(R.id.vertical_line_under_horizont);
            mImageView = (ImageView) itemView.findViewById(R.id.select_image_view);
        }

        @Override
        public void bind(RecyclerView.ViewHolder holder, int position) {
            BarEntity entity = mData.get(position);
            mBarView.setHeight(entity.getValue());
            //Bind vertical Line
            if (entity.getBottomInteger() == 1){
                mVerticalLineView.setVisibility(View.VISIBLE);
            } else {
                mVerticalLineView.setVisibility(View.GONE);
            }

            //Bind vertical line under horizont
            if(entity.getBottomInteger()%5==0){
                mVerticalLineUnderHorizont.setVisibility(View.VISIBLE);
            } else {
                mVerticalLineUnderHorizont.setVisibility(View.GONE);
            }

            //Bind date text
            if (entity.getBottomInteger() == 1 || entity.getBottomInteger()%5==0){
                mTextView.setText(entity.getBottomInteger()+"");
            } else {
                mTextView.setText("");
            }

            if (mData.get(position).isSelected()){
                mImageView.setVisibility(View.VISIBLE);
                mBarView.setSelected(true);
                mTextView.setText("");
            } else {
                mImageView.setVisibility(View.GONE);
                mBarView.setSelected(false);
            }
        }
    }

    public class HistogramListElementEmptyHolder extends CommonHolder {

        public HistogramListElementEmptyHolder(View itemView) {
            super(itemView, EMPTY_VIEW_TYPE);
        }
    }


}
