package com.example.pushApp.customviews;

import android.content.Context;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.format.DateUtils;
import android.util.AttributeSet;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.TextView;

import com.example.pushApp.R;
import com.example.pushApp.customviews.adapters.HistogramAdapter;
import com.example.pushApp.customviews.adapters.IOnClickBarListener;
import com.example.pushApp.entities.BarEntity;
import com.example.pushApp.entities.ExercizeBarEntity;
import com.example.pushApp.entities.ExersizeSet;
import com.example.pushApp.entities.ExersizeType;
import com.example.pushApp.entities.utils.ExrsizeSetDateComparator;
import com.example.pushApp.utils.Months;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.GregorianCalendar;
import java.util.List;
import java.util.Locale;

/**
 * Created by Петр on 12.12.2016.
 */

public class HistogramView extends FrameLayout {

    private static final String MONTH_FORMAT = "MMMM";
    private static final int MINIMUM_NUMBER_OF_BARS = 30;

    private SimpleDateFormat mMonthSimpleDateFormat;


    private TextView mCurrentMonthTextView;

    private RecyclerView mRecyclerView;
    private HistogramAdapter mAdapter;
    private Calendar mTempCalendar;
    private Calendar mStartCalendar;

    private int mFirstVisibleItemPos = 0;

    public HistogramView(Context context) {
        super(context);
        initialization(context);
    }

    public HistogramView(Context context, AttributeSet attrs) {
        super(context, attrs);
        initialization(context);
    }

    public HistogramView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        initialization(context);
    }

    public int getFirstVisibleItemPos() {
        return mFirstVisibleItemPos;
    }

    public HistogramAdapter getAdapter() {
        return mAdapter;
    }

    public void setData(List<ExersizeSet> data) {
        Collections.sort(data, new ExrsizeSetDateComparator());
        mAdapter.setData(prepareSortedData(data));
        mRecyclerView.scrollToPosition(mRecyclerView.getAdapter().getItemCount() - 1);
    }

    public void setOnClickBarListener(IOnClickBarListener onClickBarListener) {
        mAdapter.setOnClickBarListener(onClickBarListener);
    }

    public RecyclerView getRecyclerView() {
        return mRecyclerView;
    }

    public void scrollToPosition(int pos) {
        mRecyclerView.getLayoutManager().scrollToPosition(pos);
    }

    private void initialization(Context context) {

        mMonthSimpleDateFormat = new SimpleDateFormat(MONTH_FORMAT, new Locale("ru", "RU"));

        final View layoutFileView = inflate(context, R.layout.histogram_view_layout, null);
        addView(layoutFileView);
        mRecyclerView = (RecyclerView) layoutFileView.findViewById(R.id.recycler_view);
        LinearLayoutManager layoutManager = new LinearLayoutManager(context, LinearLayoutManager.HORIZONTAL, false);
        mRecyclerView.setLayoutManager(layoutManager);
        mAdapter = new HistogramAdapter();
        mRecyclerView.setAdapter(mAdapter);

        mTempCalendar = new GregorianCalendar();
        mStartCalendar = new GregorianCalendar();

        mRecyclerView.addOnScrollListener(new RecyclerView.OnScrollListener() {

            private int lastMonthValue = Calendar.JANUARY;

            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                mFirstVisibleItemPos = mRecyclerView.getChildAdapterPosition(mRecyclerView.getChildAt(0));
                mTempCalendar.setTimeInMillis(((ExercizeBarEntity) mAdapter.getData().get(mFirstVisibleItemPos)).getTimeInMillis());
                if (mTempCalendar.get(Calendar.MONTH) != lastMonthValue) {
                    lastMonthValue = mTempCalendar.get(Calendar.MONTH);
                    mCurrentMonthTextView.setText(Months.getMonthByCalendarPos(mTempCalendar.get(Calendar.MONTH)) + " " + mTempCalendar.get(Calendar.YEAR));
                }
            }
        });

        mCurrentMonthTextView = (TextView) layoutFileView.findViewById(R.id.current_month_text_view);
    }


    private List<BarEntity> prepareSortedData(List<ExersizeSet> sortedData) {
        List<BarEntity> result = new ArrayList<>();

        //Тут дописываем дни в данные адаптера чтоб было минимум 30 дней для экрана статистики

        fillAdapterWithMinimumAvailableDaysCount(sortedData);

        /**
         * Запонляем результирующий массив нулевыми сущностями баров
         * На данном шаге сортировка такая что первый элемент самый старый - последний элемент это самый новый
         */
        if (sortedData.size() > 0) {
            mStartCalendar.setTimeInMillis(sortedData.get(0).getEndDate());
            mStartCalendar.set(Calendar.HOUR_OF_DAY, 0);
            mStartCalendar.set(Calendar.MINUTE, 0);
            mStartCalendar.set(Calendar.SECOND, 0);
            mStartCalendar.set(Calendar.MILLISECOND, 0);
            mTempCalendar.setTimeInMillis(System.currentTimeMillis());
            int tempDay;

            int numbOfDays = Math.abs(differenceInDays(mStartCalendar, mTempCalendar));
            BarEntity entity;
            mTempCalendar.setTimeInMillis(sortedData.get(0).getEndDate());
            mTempCalendar.add(Calendar.DAY_OF_MONTH, -1);
            for (int i = 0; i <= numbOfDays; i++) {
                mTempCalendar.add(Calendar.DAY_OF_MONTH, 1);
                tempDay = mTempCalendar.get(Calendar.DAY_OF_MONTH);
                entity = new ExercizeBarEntity(0, i, tempDay);
                ((ExercizeBarEntity) entity).setTimeInMillis(mTempCalendar.getTimeInMillis());
                result.add(entity);
            }
        }

        /**
         * Тут идет объединение разных сетов в один день
         * Соответсвтенно создание сущностей BarEntity
         */
        int currentXCord = 0;
        int lastValue = 0;
        for (int i = 0; i < sortedData.size(); i++) {
            mTempCalendar.setTimeInMillis(sortedData.get(i).getEndDate());
            currentXCord = Math.abs(differenceInDays(mStartCalendar, mTempCalendar));
            lastValue = result.get(currentXCord).getValue();
            result.get(currentXCord).setValue(lastValue + sortedData.get(i).getCount());
        }

        return result;
    }

    private void fillAdapterWithMinimumAvailableDaysCount(List<ExersizeSet> sortedData) {
        ExersizeSet fakeSet;
        if (sortedData.size() == 0) {
            fakeSet = new ExersizeSet(ExersizeType.COMMON_TYPE, ExersizeSet.DEFAULT_ID, 0, System.currentTimeMillis());
            sortedData.add(0, fakeSet);
        }
        if (sortedData.get(sortedData.size()-1).getEndDate() - sortedData.get(0).getEndDate() < DateUtils.DAY_IN_MILLIS*MINIMUM_NUMBER_OF_BARS){
            mTempCalendar.setTimeInMillis(sortedData.get(sortedData.size() - 1).getEndDate());
            mTempCalendar.add(Calendar.DAY_OF_YEAR, -MINIMUM_NUMBER_OF_BARS);
            ExersizeSet startedSet = new ExersizeSet(ExersizeType.COMMON_TYPE, ExersizeSet.DEFAULT_ID, 0, mTempCalendar.getTimeInMillis());
            sortedData.add(0, startedSet);
        }
    }

    private int differenceInDays(Calendar oldCalendar, Calendar newCalendar) {

        int difInDays = (int) ((newCalendar.getTimeInMillis() - oldCalendar.getTimeInMillis()) / DateUtils.DAY_IN_MILLIS);
        int additionalDay = 0;

        if (newCalendar.get(Calendar.HOUR_OF_DAY) * DateUtils.HOUR_IN_MILLIS + newCalendar.get(Calendar.MINUTE) * DateUtils.MINUTE_IN_MILLIS + newCalendar.get(Calendar.SECOND) * DateUtils.SECOND_IN_MILLIS + newCalendar.get(Calendar.MILLISECOND) <
                oldCalendar.get(Calendar.HOUR_OF_DAY) * DateUtils.HOUR_IN_MILLIS + oldCalendar.get(Calendar.MINUTE) * DateUtils.MINUTE_IN_MILLIS + oldCalendar.get(Calendar.SECOND) * DateUtils.SECOND_IN_MILLIS + oldCalendar.get(Calendar.MILLISECOND)) {
            additionalDay = 1;
        }

        return difInDays + additionalDay;
    }
}
