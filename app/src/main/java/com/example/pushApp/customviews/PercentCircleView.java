package com.example.pushApp.customviews;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Path;
import android.util.AttributeSet;
import android.view.View;

/**
 * Created by Петр on 27.11.2016.
 */

public class PercentCircleView extends View {

    private Paint mCirclePaint;
    private Paint mRectPaint;
    private Path mCirclePath;
    private float percent=0f;

    public PercentCircleView(Context context) {
        super(context);
        initializationOfView();
    }

    public PercentCircleView(Context context, AttributeSet attrs) {
        super(context, attrs);
        initializationOfView();
    }

    public PercentCircleView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        initializationOfView();
    }


    @Override
    protected void onDraw(Canvas canvas) {
        int width = getWidth();
        int height = getHeight();

        float radius = Math.min(width/2,height/2);

        if(mCirclePath ==null){
            mCirclePath = new Path();
            mCirclePath.addCircle(width/2,height/2,radius, Path.Direction.CW);
        }

        if (!isHardwareAccelerated()){
            canvas.clipPath(mCirclePath);
            canvas.drawRect(0,height/2+radius-2*percent*radius,width,height,mRectPaint);
        }
        canvas.drawCircle(width/2,height/2,radius-1, mCirclePaint);
    }

    public float getPercent() {
        return percent;
    }

    public void setPercent(float percent) {
        this.percent = percent;
        invalidate();
    }

    public void setStrokeWidth(float width){
        mCirclePaint.setStrokeWidth(width);
        invalidate();
    }

    public void setColor(int color){
        mCirclePaint.setColor(color);
        mRectPaint.setColor(color);
    }

    private void initializationOfView(){
        mCirclePaint = new Paint();
        mCirclePaint.setStyle(Paint.Style.STROKE);
        mRectPaint = new Paint();
        mRectPaint.setStyle(Paint.Style.FILL);
    }
}
