package com.example.pushApp;

import android.app.Application;

import com.example.pushApp.dagger.CoreModule;
import com.example.pushApp.dagger.DaggerIPushAppComponent;
import com.example.pushApp.dagger.IPushAppComponent;

/**
 * @author Trikhin P O
 * @since 07.11.2016.
 */

public class PushAppApplication extends Application  {

    private IPushAppComponent sPushAppComponent;

    public IPushAppComponent getPushAppComponent() {
        return sPushAppComponent;
    }

    @Override
    public void onCreate() {
        super.onCreate();
        sPushAppComponent = DaggerIPushAppComponent.builder().coreModule(new CoreModule(this)).build();
    }
}
