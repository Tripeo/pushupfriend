package com.example.pushApp.rx;

import io.reactivex.Observer;
import io.reactivex.disposables.Disposable;

/**
 * Created by Петр on 31.05.2017.
 */

public abstract class SimpleObserver<T> implements Observer<T> {
    @Override
    public void onSubscribe(Disposable d) {

    }

    @Override
    public void onError(Throwable e) {
        e.printStackTrace();
    }

    @Override
    public void onComplete() {

    }
}
