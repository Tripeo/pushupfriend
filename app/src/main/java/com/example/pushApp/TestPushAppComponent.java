package com.example.pushApp;

import com.example.pushApp.dagger.CoreModule;
import com.example.pushApp.presentation.views.implementations.MainActivity;

import javax.inject.Singleton;

import dagger.Component;

/**
 * Created by Петр on 11.05.2017.
 */

@Component (modules = {CoreModule.class})
@Singleton
public interface TestPushAppComponent {

    void inject(MainActivity mainActivity);
}
