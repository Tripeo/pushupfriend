package com.example.pushApp;


import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.TextView;

import com.example.pushApp.entities.ExersizeSet;
import com.example.pushApp.managers.interfaces.IPreferenceManager;
import com.example.pushApp.managers.interfaces.IExersizeSetManager;

import java.util.List;

import javax.inject.Inject;


/**
 * A simple {@link Fragment} subclass.
 */
public class TotalResultFragment extends Fragment implements View.OnClickListener {

    private TextView mTotalAmountTextView;
    private TextView mMonthTotalAmountTextView;
    private TextView mTodayTextView;

    private ImageButton mToRightImageButton;
    private View.OnClickListener mOnClickListener;

    @Inject
    IExersizeSetManager mExersizeSetManager;
    @Inject
    IPreferenceManager mPreferenceManager;

    public TotalResultFragment() {
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        ((PushAppApplication)getActivity().getApplication()).getPushAppComponent().inject(this);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View resultView = inflater.inflate(R.layout.fragment_total_result, container, false);
        mTotalAmountTextView = (TextView) resultView.findViewById(R.id.total_amount_text_view);
        mMonthTotalAmountTextView = (TextView) resultView.findViewById(R.id.this_month_text_view);
        mTodayTextView = (TextView) resultView.findViewById(R.id.today_text_view);
        mToRightImageButton = (ImageButton) resultView.findViewById(R.id.to_right_button);

        bindViews();

        return resultView;
    }

    private void bindViews() {

        mToRightImageButton.setOnClickListener(this);

        //Всего
        long totalAmount = mExersizeSetManager.getTotalAmountExersizes(mPreferenceManager.readExersizeType());
        mTotalAmountTextView.setText(getString(R.string.total_amount) + " " + totalAmount);

        // В этом месяце
        long thisMonthTotalAmount = mExersizeSetManager.getMonthTotalAmountExersizes(mPreferenceManager.readExersizeType());
        mMonthTotalAmountTextView.setText(getString(R.string.this_month) + " " + thisMonthTotalAmount);

        //Сегодня
        List<ExersizeSet> todaySets = mExersizeSetManager.getDayExersizeSets(mPreferenceManager.readExersizeType(), System.currentTimeMillis());
        long todayCount = 0;
        for (ExersizeSet set : todaySets) {
            todayCount = todayCount + set.getCount();
        }
        mTodayTextView.setText(getString(R.string.today) + " " + todayCount);
    }

    @Override
    public void onClick(View v) {
        if (mOnClickListener!=null){
            mOnClickListener.onClick(v);
        }
    }

    public void setOnClickListener(View.OnClickListener onClickListener) {
        mOnClickListener = onClickListener;
    }
}
