package com.example.pushApp;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.TextView;

import com.example.pushApp.entities.ExersizeSet;
import com.example.pushApp.managers.interfaces.IPreferenceManager;
import com.example.pushApp.managers.interfaces.IExersizeSetManager;
import com.example.pushApp.presentation.views.implementations.MainActivity;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class TrainingResultActivity extends AppCompatActivity {

    @Inject
    IExersizeSetManager mExersizeSetManager;
    @Inject
    IPreferenceManager mPreferenceManager;

    @BindView(R.id.push_up_count_text_view) TextView mPushUpCountTextView;

    private int mPushCount = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        ((PushAppApplication)getApplication()).getPushAppComponent().inject(this);

        setContentView(R.layout.activity_training_result);

        ButterKnife.bind(this);

        if (getIntent()!=null && getIntent().getExtras()!=null){
            mPushCount = getIntent().getExtras().getInt(TrainingActivity.PUSH_UP_COUNT_KEY);
        }

        mPushUpCountTextView.setText(mPushCount+"");
    }

    @OnClick(R.id.save_and_return_button)
    void onSaveAndReturnButtonClicked(){
        ExersizeSet set = new ExersizeSet(mPreferenceManager.readExersizeType(),ExersizeSet.DEFAULT_ID, mPushCount,System.currentTimeMillis());
        mExersizeSetManager.writeExersizeSet(set);
        Intent intent = new Intent(this,MainActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        startActivity(intent);
    }

    @OnClick(R.id.plus_button)
    void onPlusButtonClicked(){
        mPushCount++;
        mPushUpCountTextView.setText(mPushCount+"");
    }

    @OnClick(R.id.menos_button)
    void onMenosButtonClicked(){
        mPushCount--;
        if (mPushCount<0){
            mPushCount =0;
        }
        mPushUpCountTextView.setText(mPushCount+"");
    }
}
